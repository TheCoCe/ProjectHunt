# ProjectHunt

## Requirements
This repository uses git Large File Support. If you want to clone it make sure you have git lfs installed:
- Download and install git lfs: https://git-lfs.github.com/
- Run `git lfs install`

This project is based on Unity 2020.3.20f1:
- Install the version via Unity Hub
- You can also get the version here: [2020.3.20f1](https://unity3d.com/unity/qa/lts-releases?version=2020.3.20f1)

You will need the following packages:
- TextMesh Pro (Make sure to import the "Essential Resources" via Window -> TextMesh Pro -> Import TMP Essential Resources)
- [DOTween](https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676#content) (Import via Package Manager)

You can use your preferred code editor. We recommend VS Code with the following extensions:
- [C#](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp)
- [Debugger for Unity](https://marketplace.visualstudio.com/items?itemName=Unity.unity-debug)
- [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
