using UnityEngine;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "unit_health", menuName = "ProjectHunt/Health", order = 0)]
    public class Health : ScriptableObject
    {
        [SerializeField] int maxHealth;
        public int MaxHealth => maxHealth;
        [SerializeField] int currentHealth;
        public int HealthValue
        {
            get { return currentHealth; }
            set
            {
                int newHealthValue = Mathf.Max(0, Mathf.Min(maxHealth, value));
                if (currentHealth != newHealthValue)
                {
                    currentHealth = newHealthValue;
                    OnHealthChanged?.Invoke();
                    if (currentHealth == 0) OnHealthDepleted?.Invoke();
                }
            }
        }

        public System.Action OnHealthDepleted;
        public System.Action OnHealthChanged;

        public void ApplyDamage(int dmg)
        {
            HealthValue = Mathf.Max(0, HealthValue - dmg);
        }

        public void ApplyHealing(int heal)
        {
            HealthValue = Mathf.Min(MaxHealth, HealthValue + heal);
        }
    }
}
