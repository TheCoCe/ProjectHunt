using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Cysharp.Threading.Tasks;
using System.Threading;

namespace ProjectHunt
{
    [Flags]
    public enum UnitTags
    {
        None = 0,
        Player = 1,
        Monster = 2,
        Animal = 4
    };

    public class Unit : MonoBehaviour
    {
        [SerializeField] private Faction faction;
        [SerializeField] private RuntimeUnits RuntimeUnits;

        public UnitTags tags;

        [SerializeField] private List<Ability> abilities;
        public ReadOnlyCollection<Ability> Abilities => abilities.AsReadOnly();

        private int activeAbilityIndex;
        public Ability ActiveAbility => abilities[activeAbilityIndex];

        private HexGrid parentGrid;
        public HexGrid ParentGrid { get; set; }

        public HexCell Cell => ParentGrid.GetCell(coordinate);

        public bool IsReady { get; private set; } = false;

        [SerializeField] private Health health;
        public Health Health => health;

        #region Movement
        [SerializeField] private int movementRange = 2;
        public int MovementRange => movementRange;

        [SerializeField] private float movementSpeed = 1f;
        public float MovementSpeed => movementSpeed;

        [SerializeField] private float rotationSpeed = 100f;
        public float RotationSpeed => rotationSpeed;

        [SerializeField] private int attackRange = 1;
        public int AttackRange => attackRange;

        private GridCoord coordinate;
        public GridCoord Coordinate
        {
            get => coordinate;
            set => coordinate = value;
        }

        private float orientation;
        public float Orientation
        {
            get => orientation;
            set
            {
                orientation = value;
                transform.rotation = Quaternion.Euler(0f, value, 0f);
            }
        }
        #endregion

        /// <summary>
        /// Creates an instance of each ability the unit should have
        /// </summary>
        public void Init()
        {
            for (int i = 0; i < abilities.Count; i++)
            {
                abilities[i] = ScriptableObject.Instantiate(abilities[i]);
                abilities[i].Init(this);
            }

            health = ScriptableObject.Instantiate(health);
            // TODO: health.OnHealthDepleted += SomeDeathHandlingMethod

            // Set the callback for initial ability
            activeAbilityIndex = 0;
            ActiveAbility.OnAbilityReady += OnActiveAbilityReady;
        }

        private void Awake()
        {
            faction?.AddUnit(this);
            RuntimeUnits?.AddUnit(this);
            //TODO: set initial coords
        }

        private void OnDestroy()
        {
            faction?.RemoveUnit(this);
            RuntimeUnits?.RemoveUnit(this);
        }

        public void BeginTurn()
        {
            foreach (var ability in abilities)
            {
                ability.BeginTurn();
            }
            if (!ActiveAbility.IsRunning)
                IsReady = false;
        }

        public async UniTask ExecuteTurnAsync(CancellationToken ct)
        {
            if (ActiveAbility.IsRunning)
            {
                await ActiveAbility.ExecuteActionAsync(ct);
            }
        }

        /// <summary>
        /// Sets an ability of the unit active.
        /// </summary>
        /// <returns>True for valid ability. False for invalid ability.</returns>
        public bool SetActiveAbility(Ability ability)
        {
            int idx = abilities.IndexOf(ability);
            return SetActiveAbilityIdx(idx);
        }

        public bool SetActiveAbilityIdx(int idx)
        {
            if (idx >= 0 && idx < abilities.Count && idx != activeAbilityIndex)
            {
                ActiveAbility.OnAbilityReady -= OnActiveAbilityReady;
                ActiveAbility.EndTargetSelection();
                activeAbilityIndex = idx;
                ActiveAbility.OnAbilityReady += OnActiveAbilityReady;
                ActiveAbility.SelectTargets();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Call this, when the unit is selected.
        /// </summary>
        public void Selected()
        {
            if (!ActiveAbility.IsRunning)
            {
                SetActiveAbilityIdx(0);
                ActiveAbility?.SelectTargets();
            }
        }

        private void OnActiveAbilityReady()
        {
            IsReady = true;
        }
    }
}