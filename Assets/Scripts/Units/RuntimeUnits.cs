using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "RuntimeUnits", menuName = "ProjectHunt/Runtime Units", order = 0)]
    public class RuntimeUnits : ScriptableObject
    {
        private List<Unit> units = new List<Unit>();

        public void AddUnit(Unit unit)
        {
            if (!units.Contains(unit))
                units.Add(unit);
        }

        public void RemoveUnit(Unit unit)
        {
            units.Remove(unit);
        }

        public bool HasCellUnit(GridCubeCoord coord)
        {
            if (units.Exists(x => x.Coordinate == (GridCoord)coord))
                return true;
            return false;
        }

        public bool TryGetUnit(GridCubeCoord coord, out Unit unit)
        {
            unit = null;
            if (unit = units.Find(x => x.Coordinate == (GridCoord)coord))
                return true;
            return false;
        }

        public bool TryGetUnit(string name, out Unit unit)
        {
            unit = null;
            if (unit = units.Find(x => x.name == name))
                return true;
            return false;
        }

        public IEnumerable<Unit> CreateUnitQuery()
        {
            return units.AsEnumerable();
        }
    }
}