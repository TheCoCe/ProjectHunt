using UnityEngine;
using UnityEditor;
using ProjectHunt;

[CustomEditor(typeof(Unit))]
public class UnitInspector : Editor 
{
    Unit unit;

    private void OnEnable() {
        unit = target as Unit;
    }
    public override void OnInspectorGUI() 
    {
        base.OnInspectorGUI();    
    }

    void OnSceneGUI()
    {
        unit.transform.position = (GridCoord)unit.transform.position + new Vector3(0f, unit.transform.position.y, 0f);
    }
}