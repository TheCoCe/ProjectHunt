using System.Collections.Generic;

namespace ProjectHunt
{
    public abstract class AbilityTargetData { }

    public class PathAbilityTargetData : AbilityTargetData
    {
        public List<GridCoord> path;
    }

    public class CellsAbilityTargetData : AbilityTargetData
    {
        public List<GridCoord> cells;
    }

    public class UnitsAbilityTargetData : AbilityTargetData
    {
        public List<Unit> units;
    }
}