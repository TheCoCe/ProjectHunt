using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using System.Threading;
using System;

namespace ProjectHunt
{
    public delegate IEnumerator ActionDelegate(Unit unit);

    public abstract class Ability : ScriptableObject
    {
        public System.Action OnAbilityReady;

        #region Actions
        [SerializeField] private List<Action> actions;
        private int activeActionIdx = 0;
        public bool IsRunning { get; protected set; } = false;
        public Action ActiveAction => activeActionIdx < actions.Count ? actions[activeActionIdx] : null;
        #endregion

        #region Cooldown
        [SerializeField] private int maxCooldown = 0;
        private int currentCooldown = 0;
        public bool OnCooldown => currentCooldown > 0;
        #endregion

        protected Unit unit;
        protected AbilityTargetData targetData;
        public bool HasTargetData => targetData != null;

        /// <summary>
        /// Creates an instance of each action
        /// </summary>
        public virtual void Init(Unit unit)
        {
            this.unit = unit;

            for (int i = 0; i < actions.Count; i++)
            {
                actions[i] = ScriptableObject.Instantiate(actions[i]);
                actions[i].Init();
            }
        }

        /// <summary>
        /// Sets targetData
        /// </summary>
        /// <param name="unit"></param>
        public abstract void SelectTargets();

        /// <summary>
        /// Ends target selection
        /// </summary>
        /// <param name="unit"></param>
        public abstract void EndTargetSelection();

        /// <summary>
        /// Handles the ability state. Resets the ability if all actions were executed and handles the ability cooldown if one is set.
        /// </summary>
        /// <param name="forceReset">Force a reset even if the ability might still be running</param>
        public void BeginTurn(bool forceReset = false)
        {
            // reset ability after all actions have been executed or for a forced reset
            if (activeActionIdx >= actions.Count || forceReset)
            {
                IsRunning = false;
                activeActionIdx = 0;
                targetData = null;
                currentCooldown = forceReset ? 0 : maxCooldown;
            }

            if (OnCooldown)
                currentCooldown--;
        }

        public void SetActionList(List<Action> actionList)
        {
            this.actions = actionList;
        }

        /// <summary>
        /// Callback thats called when the targetData is ready
        /// </summary>
        protected virtual void OnTargetData(AbilityTargetData targetData)
        {
            this.targetData = targetData;
            if (targetData != null)
            {
                foreach (Action action in actions)
                    action.TargetData = targetData;
            }
        }

        public async UniTask ExecuteActionAsync(CancellationToken ct)
        {
            await ActiveAction.ExecuteActionAsync(ct, unit);
            activeActionIdx++;
        }
    }
}