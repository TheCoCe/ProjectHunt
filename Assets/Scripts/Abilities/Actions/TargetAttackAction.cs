using UnityEngine;
using Cysharp.Threading.Tasks;
using System.Threading;
using System.Collections.Generic;
using System.Linq;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "target_attack_act", menuName = "ProjectHunt/Actions/TargetAttackAction", order = 0)]
    public class TargetAttackAction : Action
    {
        [SerializeField] private int damage;

        public override async UniTask ExecuteActionAsync(CancellationToken ct, Unit unit = null)
        {
            foreach (Unit target in (targetData as UnitsAbilityTargetData).units)
                target.Health.ApplyDamage(damage);
        }
    }
}