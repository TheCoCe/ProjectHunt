using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using System.Threading;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "new_move_single_action", menuName = "ProjectHunt/Actions/MoveSingleAction", order = 0)]
    public class MoveSingleAction : Action
    {
        public override async UniTask ExecuteActionAsync(CancellationToken ct, Unit unit)
        {
            List<GridCoord> path = ((PathAbilityTargetData)targetData).path;
            int range = unit.MovementRange;

            if (path.Count > 1)
            {
#if DEBUG
                for (int i = 0; i < path.Count && i < range; i++)
                {
                    Drawer.DrawPosition(path[i], ColorUtils.LerpHSV(Color.red, Color.green, (float)i / (float)path.Count), 1f, 10f);
                }
#endif
                Vector3 a, b, c = path[0];
                await LookTowardsAsync(path[1], unit, ct);

                float t = Time.deltaTime * unit.MovementSpeed;
                for (int i = 1; i < path.Count && i <= range; i++)
                {
                    a = c;
                    b = path[i - 1];
                    c = (b + (path[i] - b) * 0.5f);
#if DEBUG
                    Drawer.DrawLine(a, b, 10f, ColorUtils.LerpHSV(Color.red, Color.green, (float)i / (float)path.Count));
                    Drawer.DrawLine(b, c, 10f, ColorUtils.LerpHSV(Color.red, Color.green, (float)i / (float)path.Count));
#endif
                    for (; t < 1f; t += Time.deltaTime * unit.MovementSpeed)
                    {
                        unit.transform.position = GetGroundHeight(Bezier.GetPoint(a, b, c, t));
                        Vector3 d = Bezier.GetTangent(a, b, c, t);
                        d.y = 0f;
                        unit.transform.rotation = Quaternion.LookRotation(d);
                        await UniTask.Yield(PlayerLoopTiming.Update, ct);
                    }
                    t -= 1f;
                }

                int endIdx = Mathf.Min(range, path.Count - 1);

                a = c;
                b = path[endIdx];
                c = b;
                for (; t < 1f; t += Time.deltaTime * unit.MovementSpeed)
                {
                    unit.transform.position = GetGroundHeight(Bezier.GetPoint(a, b, c, t));
                    Vector3 d = Bezier.GetTangent(a, b, c, t);
                    d.y = 0f;
                    unit.transform.rotation = Quaternion.LookRotation(d);
                    await UniTask.Yield(PlayerLoopTiming.Update, ct);
                }

                unit.transform.position = GetGroundHeight(path[endIdx]);
                unit.Coordinate = path[endIdx];
                unit.Orientation = unit.transform.rotation.eulerAngles.y;

                path.RemoveRange(0, range);
            }
        }

        private IEnumerator LookTowards(Vector3 point, Unit unit)
        {
            point.y = unit.transform.position.y;
            Quaternion fromRotation = unit.transform.rotation;
            Quaternion toRotation = Quaternion.LookRotation(point - unit.transform.position);

            float angle = Quaternion.Angle(fromRotation, toRotation);
            if (angle > 0f)
            {
                float speed = unit.RotationSpeed / angle;

                float t = Time.deltaTime * speed;
                for (; t < 1f; t += Time.deltaTime * speed)
                {
                    unit.transform.rotation = Quaternion.Slerp(fromRotation, toRotation, t);
                    yield return null;
                }

                unit.transform.LookAt(point);
                unit.Orientation = unit.transform.rotation.eulerAngles.y;
            }
        }

        private async UniTask LookTowardsAsync(Vector3 point, Unit unit, CancellationToken ct)
        {
            point.y = unit.transform.position.y;
            Quaternion fromRotation = unit.transform.rotation;
            Quaternion toRotation = Quaternion.LookRotation(point - unit.transform.position);

            float angle = Quaternion.Angle(fromRotation, toRotation);
            if (angle > 0f)
            {
                float speed = unit.RotationSpeed / angle;

                float t = Time.deltaTime * speed;
                for (; t < 1f; t += Time.deltaTime * speed)
                {
                    unit.transform.rotation = Quaternion.Slerp(fromRotation, toRotation, t);
                    await UniTask.Yield(PlayerLoopTiming.Update, ct);
                }

                unit.transform.LookAt(point);
                unit.Orientation = unit.transform.rotation.eulerAngles.y;
            }
        }

        private Vector3 GetGroundHeight(Vector3 position)
        {
            RaycastHit hit;
            if (Physics.Raycast(new Ray(position + Vector3.up * 1000f, -Vector3.up), out hit, Mathf.Infinity, LayerMask.GetMask("Ground")))
            {
                position.y = hit.point.y;
                return position;
            }

            return position;
        }
    }
}