using UnityEngine;
using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using System.Threading;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "new_container_action", menuName = "ProjectHunt/Actions/Container Action", order = 0)]
    public class ContainerAction : Action
    {
        [SerializeField] protected List<Action> actions;

        public override AbilityTargetData TargetData
        {
            set
            {
                targetData = value;
                foreach (var action in actions)
                    action.TargetData = value;
            }
        }

        public override void Init()
        {
            for (int i = 0; i < actions.Count; i++)
            {
                actions[i] = ScriptableObject.Instantiate(actions[i]);
            }
        }

        public override async UniTask ExecuteActionAsync(CancellationToken ct, Unit unit = null)
        {
            UniTask[] tasks = new UniTask[actions.Count];

            for (int i = 0; i < actions.Count; i++)
            {
                tasks[i] = actions[i].ExecuteActionAsync(ct, unit);
            }

            await UniTask.WhenAll(tasks);
        }
    }
}