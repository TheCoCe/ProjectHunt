using UnityEngine;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using System.Threading;
using System;

namespace ProjectHunt
{
    public abstract class Action : ScriptableObject
    {
        protected AbilityTargetData targetData;
        public virtual AbilityTargetData TargetData { set { targetData = value; } }
        public virtual void Init() { }
        public abstract UniTask ExecuteActionAsync(CancellationToken ct, Unit unit = null);
    }
}