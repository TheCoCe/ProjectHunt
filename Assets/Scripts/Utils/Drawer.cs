using UnityEngine;

public static class Drawer
{
    public static void DrawLine(Vector3 start, Vector3 end, float duration, Color color)
    {
        Debug.DrawLine(start, end, color, duration);
    }

    public static void DrawLine(Vector3 start, Vector3 end, float duration)
    {
        DrawLine(start, end, duration, Color.red);
    }

    public static void DrawTransform(Transform transform, Color color, float size, float duration)
    {
        Debug.DrawLine(transform.position, transform.position + transform.up * size, color, duration);
        Debug.DrawLine(transform.position, transform.position + transform.right * size, color, duration);
        Debug.DrawLine(transform.position, transform.position + transform.forward * size, color, duration);
    }

    public static void DrawTransform(Transform transform, float size, float duration)
    {
        Debug.DrawLine(transform.position, transform.position + transform.up * size, Color.green, duration);
        Debug.DrawLine(transform.position, transform.position + transform.right * size, Color.red, duration);
        Debug.DrawLine(transform.position, transform.position + transform.forward * size, Color.blue, duration);
    }

    public static void DrawPosition(Vector3 position, Color color, float size, float duration)
    {
        Debug.DrawLine(position, position + Vector3.up * size, color, duration);
        Debug.DrawLine(position, position + Vector3.right * size, color, duration);
        Debug.DrawLine(position, position + Vector3.forward * size, color, duration);
    }
}