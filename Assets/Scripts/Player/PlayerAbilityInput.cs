using UnityEngine;

namespace ProjectHunt
{
    public abstract class PlayerAbilityInput : ScriptableObject
    {
        public abstract InputContextID Context { get; }
        protected Unit unit;

        protected System.Action<AbilityTargetData> OnInputFinished;

        public virtual void Initialize(Unit unit, System.Action<AbilityTargetData> finishedCallback)
        {
            this.unit = unit;
            Debug.Assert(unit != null, "Unit can't be null!");

            // Only one callback can be active at a time
            OnInputFinished = finishedCallback;

            InputManager.Instance.SetContextState(Context, true);
            InputManager.Instance.RegisterCallback(Context, HandleInput);
        }

        public virtual void Deinitialize()
        {
            unit = null;
            OnInputFinished = null;

            InputManager.Instance.SetContextState(Context, false);
            InputManager.Instance.UnregisterCallback(Context, HandleInput);
        }

        public abstract MappedInput HandleInput(MappedInput input);
    }
}