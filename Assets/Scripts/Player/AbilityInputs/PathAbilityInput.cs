using UnityEngine;
using System.Collections.Generic;
using System;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "new_path_ability_input", menuName = "ProjectHunt/Ability Inputs/PathAbilityInput", order = 0)]
    public class PathAbilityInput : PlayerAbilityInput
    {
        [SerializeField] private Color startColor;
        [SerializeField] private Color targetColor;

        public override InputContextID Context { get { return InputContextID.UnitMovement; } }

        private List<GridCoord> path;
        private List<HexSprite> pathSprites;

        public override void Initialize(Unit unit, Action<AbilityTargetData> finishedCallback)
        {
            base.Initialize(unit, finishedCallback);

            path = null;
            ClearPathSprites();
            pathSprites = null;
        }

        public override void Deinitialize()
        {
            base.Deinitialize();

            ClearPathSprites();
            pathSprites = null;
        }

        public override MappedInput HandleInput(MappedInput input)
        {
            // find path if we have no path 
            if (input.hoveredCell)
            {
                // get new path if last element of path not equal to hovered cell
                if (path == null || (path.Count > 0 && input.hoveredCell.coordinates != path[path.Count - 1]))
                {
                    // get path
                    path = unit.ParentGrid.Pathfinding.AStar(unit.Coordinate, input.hoveredCell.coordinates);
                    if (path != null)
                    {
                        // reset path sprites
                        ClearPathSprites();

                        pathSprites = SpriteManager.Instance.ReserveSprite(SpriteType.CellHighlight, path.Count);

                        // display path
                        for (int i = 1; i < path.Count; i++)
                        {
                            float colorValue = ((i + 1) / unit.MovementRange) * (1f / (path.Count - 1) * unit.MovementRange);
                            // Debug.Log("i: " + i + ", Value:" + colorValue);
                            pathSprites[i].Display(unit.ParentGrid.GetCell(path[i]), Color.Lerp(startColor, targetColor, colorValue));
                        }
                    }
                }

                // commit input
                if (input.clickedCell)
                {
                    // construct movement path
                    PathAbilityTargetData result = new PathAbilityTargetData();
                    result.path = new List<GridCoord>(path);

                    // return callback
                    OnInputFinished.Invoke(result);

                    input.clickedCell = null;
                }
            }
            return input;
        }

        private void ClearPathSprites()
        {
            if (pathSprites != null)
                foreach (var sprite in pathSprites)
                    if (sprite != null) sprite.Free();
        }
    }
}