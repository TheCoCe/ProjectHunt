using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "new_player_ability", menuName = "ProjectHunt/Abilities/Player Ability", order = 0)]
    public class PlayerAbility : Ability
    {
        [SerializeField] private PlayerAbilityInput abilityInput;

        public override void SelectTargets()
        {
            if (!IsRunning)
                abilityInput.Initialize(unit, OnTargetData);
        }

        public override void EndTargetSelection()
        {
            if (!IsRunning)
                abilityInput.Deinitialize();
        }

        protected override void OnTargetData(AbilityTargetData targetData)
        {
            base.OnTargetData(targetData);
            abilityInput.Deinitialize();
            IsRunning = true;
            OnAbilityReady.Invoke();
        }
    }
}