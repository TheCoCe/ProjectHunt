namespace ProjectHunt
{
    public class MappedInput
    {
        // all members default values should be null
        public HexCell clickedCell = null;
        public Unit clickedUnit = null;
        public HexCell hoveredCell = null;

        public static MappedInput UseInput(MappedInput x, MappedInput y)
        {
            MappedInput tmp = new MappedInput();

            // TODO: extend with more members
            tmp.clickedCell = y.clickedCell ? x.clickedCell : null;
            tmp.clickedUnit = y.clickedUnit ? x.clickedUnit : null;
            tmp.hoveredCell = y.hoveredCell ? x.hoveredCell : null;

            return tmp;
        }

        public void OverrideValues(MappedInput other)
        {
            // TODO: extend with more members
            this.clickedCell = other.clickedCell;
            this.clickedUnit = other.clickedUnit;
            this.hoveredCell = other.hoveredCell;
        }

        public bool IsAllNull()
        {
            // TODO: extend with more members
            if (!clickedCell && !clickedUnit && !hoveredCell)
                return true;

            return false;
        }

        public void SetAllNull()
        {
            // TODO: extend with more members
            clickedCell = null;
            clickedUnit = null;
            hoveredCell = null;
        }
    }
}