using UnityEngine;
using System.Collections.Generic;

namespace ProjectHunt
{
    public class InputManager : SingletonBehaviour<InputManager>
    {
        public delegate MappedInput InputCallback(MappedInput input);

        private class InputContextData
        {
            public bool state;
            public InputCallback callback;
        }

        [SerializeField] private InputContextSettings contextSettings;
        [SerializeField] private HexGrid grid;
        [SerializeField] private RuntimeUnits runtimeUnits;

        // context data
        private Dictionary<InputContextID, InputContextData> contextData;

        // internal data
        private bool newInput = false;
        private MappedInput mappedInput, usedInput;
        private HexCell cellHit;

        private void Awake()
        {
            // initialize context data 
            contextData = new Dictionary<InputContextID, InputContextData>(contextSettings.contexts.Length);

            for (int i = 0; i < contextSettings.contexts.Length; i++)
                contextData.Add(contextSettings.contexts[i].Id, new InputContextData()
                {
                    callback = null,
                    state = contextSettings.contexts[i].isDefaultActive
                });

            mappedInput = new MappedInput();
            usedInput = new MappedInput();
        }

        private void Update()
        {
            // get inputs
            cellHit = null;
            if (grid.RaycastGround(out cellHit))
            {
                mappedInput.hoveredCell = cellHit;
                newInput = true;
            }

            if (Input.GetMouseButtonDown(0))
            {
                // raycast against unit colliders
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, float.MaxValue, LayerMask.GetMask("Unit")))
                {
                    mappedInput.clickedUnit = hit.collider.gameObject.GetComponent<Unit>();
                    newInput = true;
                }

                if (cellHit)
                {
                    // raycast against grid
                    mappedInput.clickedCell = cellHit;

                    // get cell on unit if raycast against units was unsuccessful
                    if (!mappedInput.clickedUnit)
                        runtimeUnits.TryGetUnit(cellHit.coordinates, out mappedInput.clickedUnit);

                    newInput = true;
                }
            }

            // send input to contexts if new input available
            if (newInput)
            {
                DispatchInput();
                mappedInput.SetAllNull();
                newInput = false;
            }
        }

        /// <summary>
        /// Registeres a function as callback for an input context.
        /// </summary>
        /// <param name="contextID">ID of the input context</param>
        /// <param name="func">Function to call. Has to take an object of type MappedInput.
        /// Members of the MappedInput should be set to null, if used. No other context will gets access to that data. 
        /// Other callbacks of the same context still get access.</param>
        public void RegisterCallback(InputContextID contextID, InputCallback func)
        {
            contextData[contextID].callback = contextData[contextID].callback == null ? func : contextData[contextID].callback + func;
        }

        /// <summary>
        /// Removes a function from the invokation list of an input context.
        /// </summary>
        /// <param name="contextID">ID of the input context</param>
        /// <param name="func">Function to remove from callbacks</param>
        public void UnregisterCallback(InputContextID contextID, InputCallback func)
        {
            contextData[contextID].callback -= func;
        }

        /// <summary>
        /// Sets the current state of an input context.
        /// </summary>
        /// <param name="contextID">ID of the input context</param>
        /// <param name="newState">New state of the input context</param>
        public void SetContextState(InputContextID contextID, bool newState)
        {
            contextData[contextID].state = newState;
        }

        /// <summary>
        /// Returns if given input context is currently active. UI actions of this context can use this data to decide on.
        /// </summary>
        /// <param name="contextID">ID of the input context</param>
        /// <returns>Current state of the input context</returns>
        public bool IsContextActive(InputContextID contextID)
        {
            return contextData[contextID].state;
        }

        private void DispatchInput()
        {
            usedInput.SetAllNull();
            for (int i = 0; i < contextSettings.contexts.Length; i++)
            {
                if (contextData[contextSettings.contexts[i].Id].state && contextData[contextSettings.contexts[i].Id].callback != null)
                {
                    foreach (InputCallback callback in contextData[contextSettings.contexts[i].Id].callback.GetInvocationList())
                    {
                        usedInput = MappedInput.UseInput(mappedInput, callback(mappedInput));
                    }
                    if (usedInput.IsAllNull())
                        return;
                    mappedInput.OverrideValues(usedInput);
                }
            }
        }
    }
}