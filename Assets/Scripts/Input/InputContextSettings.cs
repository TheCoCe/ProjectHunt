using System;
using UnityEngine;

// public enum InputType
// {
//     HexClicked
// }
namespace ProjectHunt
{
    public enum InputContextID
    {
        Debugging,
        UnitSelection,
        TargetSelection,
        UnitMovement
    }

    [Serializable]
    public struct InputContext
    {
        public InputContextID Id;
        public bool isDefaultActive;

        // extensible for filtering input later 
        //public InputType[] validInputs;
    }

    [CreateAssetMenu(fileName = "InputContextSettings", menuName = "Input/InputContextSettings", order = 0)]
    public class InputContextSettings : ScriptableObject
    {
        public InputContext[] contexts;
    }
}