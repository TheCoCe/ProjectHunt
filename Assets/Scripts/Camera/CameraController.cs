﻿using UnityEngine;

public class CameraController : MonoBehaviour
{

    #region Data
    public AnimationCurve curve;
    public Transform cameraTransform;
    public float speed;
    public float smoothingTime = 0.125f;
    [Range(0f, 1f)]
    public float cameraRotationDelay = 0.5f;
    [Range(0f, 10f)]
    public float mouseSensitivity = 10;
    [Range(0.01f, 1f)]
    public float minMoveSpeed = 0.1f;
    [Range(0.01f, 1f)]
    public float rotationSensitivity = 0.5f;
    public bool mouseCameraMovement = false;

    private float Zoom;
    private static int Boundary = 100;
    private float min, max;
    private float MouseX;
    private float rotationAxis;
    private Vector3 direction;
    private Vector3 velocity;
    private Vector3 desiredPosition = Vector3.zero;
    private bool center = false;
    #endregion

    #region Properties
    public float MaxKey
    {
        get
        {
            return max;
        }
    }
    public float MinKey
    {
        get
        {
            return min;
        }
    }
    public float MaxValue
    {
        get;
        private set;
    }
    public float MinValue
    {
        get;
        private set;
    }
    #endregion

    #region Events
    private void OnEnable()
    {
        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
    }

    private void Start()
    {
        Keyframe[] keys = curve.keys;
        if (keys.Length > 0)
        {
            min = max = keys[0].time;
            MaxValue = keys[0].value;
            for (int i = 0; i < keys.Length; i++)
            {
                if (keys[i].time < min)
                {
                    min = keys[i].time;
                    MaxValue = keys[i].value;
                }
                if (keys[i].time > max)
                {
                    max = keys[i].time;
                    MinValue = keys[i].value;
                }
            }
        }
        Zoom = min;

        if (cameraTransform == null)
        {
            cameraTransform = GetComponentInChildren<Transform>();
        }
    }

    private void Update()
    {
        direction.x = Input.GetAxis("Horizontal");
        direction.z = Input.GetAxis("Vertical");
        rotationAxis = Input.GetAxis("CameraRotation");
    }

    private void LateUpdate()
    {
        #region Camera Movement
        float percent = (Zoom - min) / (max - min);
        if (!Input.GetMouseButton(2))
        {
            if (mouseCameraMovement)
            {
                float distance = Input.mousePosition.x - (Screen.width - Boundary);
                if (distance > 0)
                {
                    direction.x = (distance / Boundary);
                }
                distance = Boundary - Input.mousePosition.x;
                if (distance > 0)
                {
                    direction.x = -(distance / Boundary);
                }
                distance = Input.mousePosition.y - (Screen.height - Boundary);
                if (distance > 0)
                {
                    direction.z = (distance / Boundary);
                }
                distance = Boundary - Input.mousePosition.y;
                if (distance > 0)
                {
                    direction.z = -(distance / Boundary);
                }
            }

            desiredPosition += (transform.rotation * direction) * Time.deltaTime * speed * (1 - percent + minMoveSpeed);
        }

        Vector3 smoothedPosition;
        if (!center)
        {
            smoothedPosition = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothingTime);
            cameraTransform.LookAt(Vector3.Lerp(transform.position, desiredPosition, cameraRotationDelay));
        }
        else
        {
            smoothedPosition = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, 0.125f);
            if (Vector3.Distance(transform.position, desiredPosition) < 0.05f)
                center = false;
        }
        transform.position = smoothedPosition;
        #endregion

        #region Camera Zoom
        Zoom += Input.GetAxis("Mouse ScrollWheel") * mouseSensitivity;
        Zoom = Mathf.Clamp(Zoom, min, max);
        cameraTransform.localPosition = Vector3.Lerp(cameraTransform.localPosition, new Vector3(cameraTransform.localPosition.x, curve.Evaluate(Zoom), Zoom), .02f);
        #endregion


        if (!MouseRotation())
        {
            transform.Rotate(new Vector3(0f, rotationAxis * rotationSensitivity, 0f));
        }
        MouseX = Input.mousePosition.x;
    }
    #endregion

    #region Methods
    // Handles the Mouse Rotation
    private bool MouseRotation()
    {
        if (Input.GetMouseButton(2) && Input.mousePosition.x != MouseX)
        {
            transform.Rotate(new Vector3(0.0f, (Input.mousePosition.x - MouseX) * Time.deltaTime * mouseSensitivity, 0.0f));
            return true;
        }
        return false;
    }

    public void CenterCamera(Vector3 newPosition)
    {
        center = true;
        desiredPosition = newPosition;
    }
    #endregion
}
