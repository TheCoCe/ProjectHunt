// Source: https://gist.github.com/mandarinx/ed733369fbb2eea6c7fa9e3da65a0e17
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MeshFilter))]
public class NormalsVisualizer : Editor
{

    private Mesh mesh;

    private bool _enabled = false;

    void OnEnable()
    {
        MeshFilter mf = target as MeshFilter;
        if (mf != null)
        {
            mesh = mf.sharedMesh;
        }
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        _enabled = EditorGUILayout.Toggle("Show normals", _enabled);
    }

    void OnSceneGUI()
    {
        if (mesh == null)
        {
            return;
        }

        if (!_enabled)
        {
            return;
        }

        Handles.matrix = (target as MeshFilter).transform.localToWorldMatrix;
        Handles.color = Color.yellow;
        Vector3[] verts = mesh.vertices;
        Vector3[] normals = mesh.normals;
        int len = mesh.vertexCount;

        Color[] colors = { Color.blue, Color.cyan, Color.gray, Color.green, Color.yellow, Color.magenta, Color.red };

        for (int i = 0; i < len; i++)
        {
            Handles.color = colors[i % colors.Length];
            Handles.DrawLine(verts[i], verts[i] + Vector3.Normalize(normals[i]));
        }
    }
}