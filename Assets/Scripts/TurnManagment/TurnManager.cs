using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using System.Threading;
using System;

namespace ProjectHunt
{
    public class TurnManager : MonoBehaviour
    {
        [SerializeField] private List<Faction> factions;

        // action execution variables
        private int actionCount;
        private int actionsFinished;

        private int turnCounter = 0;

        private int currentFactionIdx;
        public Faction NextFaction
        {
            get { return factions[currentFactionIdx]; }
        }

        CancellationTokenSource cts = new CancellationTokenSource();

        private void Start()
        {
            foreach (Faction faction in factions)
                faction.Init();

            // TODO: start turn sequence (should be called from somewhere else)
            //StartTurnCycle();
            //StartAsyncTurnCycle();
        }

        // Async loop
        public void StartAsyncTurnCycle()
        {
            TurnCycle(cts.Token).Forget();
        }

        private async UniTaskVoid TurnCycle(CancellationToken ct)
        {
            while (true)
            {
                Debug.Log($"New Turn ({turnCounter})");
                turnCounter++;
                await PlanFactionTurnsAsync(ct);
                await ExecuteFactionTurnsAsync(ct);
                await UniTask.WaitForEndOfFrame(this);
            }
        }

        private async UniTask PlanFactionTurnsAsync(CancellationToken ct)
        {
            foreach (var faction in factions)
            {
                faction.BeginTurn();
                await faction.PlanTurnAsync(ct);
                faction.EndTurn();
            }
        }

        private async UniTask ExecuteFactionTurnsAsync(CancellationToken ct)
        {
            UniTask[] tasks = new UniTask[factions.Count];
            for (int i = 0; i < factions.Count; i++)
            {
                tasks[i] = factions[i].ExecuteUnitTurnsAsync(ct);
            }

            await UniTask.WhenAll(tasks);
        }

        void OnDestroy()
        {
            cts.Cancel();
            cts.Dispose();
        }
    }
}