using UnityEngine;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using System.Threading;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "PlayerFaction", menuName = "ProjectHunt/PlayerFaction", order = 0)]
    public class PlayerFaction : Faction
    {
        #region DATA
        private Unit selectedUnit = null;
        private HexSprite unitSelectionSprite;
        public System.Action<Unit> OnUnitSelectedEvent;
        #endregion

        #region ClassStuff
        public override void Init()
        {
            unitSelectionSprite = SpriteManager.Instance.ReserveSprite(SpriteType.CellHighlight);

            InputManager.Instance.RegisterCallback(InputContextID.UnitSelection, OnUnitSelected);

            selectedUnit = null;
        }

        public void OnDestroy()
        {
            InputManager.Instance.UnregisterCallback(InputContextID.UnitSelection, OnUnitSelected);
        }

        public override void BeginTurn()
        {
            base.BeginTurn();

            InputManager.Instance.SetContextState(InputContextID.UnitSelection, true);

            // FIXME: dirty hack as we don't initially have units yet so select default unit
            if (selectedUnit == null)
            {
                if (units.Count > 0)
                    SelectUnit(units[0]);
            }
            else
            {
                if (selectedUnit.ActiveAbility != null && !selectedUnit.ActiveAbility.IsRunning)
                    SelectUnit(selectedUnit);
                else
                    foreach (Unit unit in units)
                        if (unit.ActiveAbility && !unit.ActiveAbility.IsRunning)
                            SelectUnit(unit);

            }
        }

        public override System.Collections.IEnumerator PlanTurn()
        {
            Debug.Log(name + " Turn");

            bool allUnitsFinished = false;
            while (!allUnitsFinished && units.Count > 0)
            {
                // FIXME: this is a hack for as long as we don't initially have units as a player faction
                allUnitsFinished = true;
                for (int i = 0; i < units.Count && allUnitsFinished; i++)
                {
                    allUnitsFinished &= units[i].IsReady;
                }
                yield return null;
            }

            yield return new WaitForEndOfFrame();
            // yield return new WaitUntil(() => Input.GetKeyDown("enter"));

            Debug.Log("Player Faction invoke OnPlanningFinished");
            OnPlanningFinished.Invoke();
        }

        public override async UniTask PlanTurnAsync(CancellationToken ct)
        {
            Debug.Log(name + " Turn");

            while (!units.TrueForAll(x => x.IsReady) && units.Count > 0)
            {
                await UniTask.Yield(PlayerLoopTiming.Update, ct);
            }
        }

        public override void EndTurn()
        {
            Debug.Log(name + " End Turn");

            unitSelectionSprite.ActiveInHierarchy = false;

            InputManager.Instance.SetContextState(InputContextID.UnitSelection, false);
            InputManager.Instance.SetContextState(InputContextID.UnitMovement, false);
        }

        public MappedInput OnUnitSelected(MappedInput input)
        {
            if (input.clickedUnit && input.clickedUnit != selectedUnit && units.Contains(input.clickedUnit))
            {
                SelectUnit(input.clickedUnit);

                input.clickedUnit = null;
                input.clickedCell = null;
            }
            return input;
        }

        private void SelectUnit(Unit unit)
        {
            selectedUnit = unit;
            if (selectedUnit == null)
            {
                OnUnitSelectedEvent?.Invoke(unit);
                return;
            }

            selectedUnit.Selected();
            OnUnitSelectedEvent?.Invoke(unit);
            unitSelectionSprite.Display(unit.Cell, Color.blue);
            InputManager.Instance.SetContextState(InputContextID.UnitMovement, true);
        }
        #endregion
    }
}