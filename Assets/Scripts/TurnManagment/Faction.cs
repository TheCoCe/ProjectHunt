using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Cysharp.Threading.Tasks;
using System.Threading;
using System;

namespace ProjectHunt
{
    public abstract class Faction : ScriptableObject
    {
        [SerializeField]
        [Tooltip("These tags are applied to all units of this faction.")]
        private UnitTags factionTags;

        protected List<Unit> units = new List<Unit>();

        public System.Action OnPlanningFinished;

        public UnitTags FactionTags { get { return factionTags; } }

        public virtual void Init() { }

        public virtual void BeginTurn()
        {
            foreach (Unit unit in units)
                unit.BeginTurn();
        }

        public abstract IEnumerator PlanTurn();
        public virtual void EndTurn() { }

        public abstract UniTask PlanTurnAsync(CancellationToken ct);

        public virtual async UniTask ExecuteUnitTurnsAsync(CancellationToken ct)
        {
            UniTask[] tasks = new UniTask[units.Count];
            for (int i = 0; i < units.Count; i++)
            {
                tasks[i] = units[i].ExecuteTurnAsync(ct);
            }

            await UniTask.WhenAll(tasks);
        }

        public virtual void AddUnit(Unit unit)
        {
            if (!units.Contains(unit))
            {
                units.Add(unit);
                unit.tags |= factionTags;
            }
        }

        public virtual void RemoveUnit(Unit unit)
        {
            units.Remove(unit);
        }
    }
}