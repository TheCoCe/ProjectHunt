using System.Collections.Generic;
using UnityEngine;
using Cysharp.Threading.Tasks;
using System.Threading;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "AIFaction", menuName = "ProjectHunt/AIFaction", order = 0)]
    public class AIFaction : Faction
    {
        private List<AIController> aiControllers;
        public override void AddUnit(Unit unit)
        {
            base.AddUnit(unit);

            if (aiControllers == null)
                aiControllers = new List<AIController>();

            AIController controller = unit.GetComponent<AIController>();
            if (!aiControllers.Contains(controller))
                aiControllers.Add(controller);
        }

        public override void RemoveUnit(Unit unit)
        {
            base.RemoveUnit(unit);

            aiControllers.Remove(unit.GetComponent<AIController>());
        }

        public override System.Collections.IEnumerator PlanTurn()
        {
            Debug.Log(name + " Turn");

            foreach (var controller in aiControllers)
            {
                controller.PlanTurn();
            }

            yield return null;

            Debug.Log("AIFaction invoke OnPlanningFinished");
            OnPlanningFinished.Invoke();
        }

        public override async UniTask PlanTurnAsync(CancellationToken ct)
        {
            Debug.Log(name + " Turn");

            foreach (var controller in aiControllers)
            {
                // TODO: can we async AI turns?
                controller.PlanTurn();
            }

            // FIXME: do we even need to yield here?
            await UniTask.Yield(PlayerLoopTiming.Update, ct);
        }
    }
}