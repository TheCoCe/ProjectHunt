using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace ProjectHunt
{
    public class Pathfinding
    {
        private class PathfindingData : IHeapItem<PathfindingData>
        {
            public GridCoord coord;
            public int distanceFromStart;
            public int heuristicToTarget;
            public PathfindingData pathPredecessor;

            public int TotalDistance
            {
                get { return distanceFromStart + heuristicToTarget; }
            }

            public int HeapIndex { get; set; }

            public PathfindingData(GridCoord coord, int distanceFromStart, int heuristicToTarget)
            {
                this.coord = coord;
                this.distanceFromStart = distanceFromStart;
                this.heuristicToTarget = heuristicToTarget;
                this.pathPredecessor = null;
            }

            public PathfindingData(GridCoord coord, int distanceFromStart, int heuristicToTarget, PathfindingData pathPredecessor)
                : this(coord, distanceFromStart, heuristicToTarget)
            {
                this.pathPredecessor = pathPredecessor;
            }

            public int CompareTo(PathfindingData other)
            {
                int compare = TotalDistance.CompareTo(other.TotalDistance);
                if (compare == 0)
                {
                    compare = heuristicToTarget.CompareTo(other.heuristicToTarget);
                }
                return -compare;
            }

            public override int GetHashCode()
            {
                return coord.GetHashCode();
            }
        }

        private HexGrid grid;

        // calculation variables
        Queue<PathfindingData> frontier;
        Heap<PathfindingData> openSet;
        HashSet<PathfindingData> closedSet;
        GridCubeCoordBlockDirFlags blockDirMask;
        GridCubeCoordDirection dir;
        GridCubeCoord neighbourCoord;
        int newDistance;
        int cellCountX, cellCountY;
        PathfindingData[] pathfindingData;

        public Pathfinding(HexGrid grid, int cellCountX, int cellCountY)
        {
            frontier = new Queue<PathfindingData>();
            openSet = new Heap<PathfindingData>(grid.CellCount());
            closedSet = new HashSet<PathfindingData>();

            this.cellCountX = cellCountX;
            this.cellCountY = cellCountY;

            // Create a grid of PathfindingData thats the same size as the grid
            pathfindingData = new PathfindingData[cellCountX * cellCountY];
            for (int x = 0; x < cellCountX; x++)
            {
                for (int y = 0; y < cellCountY; y++)
                {
                    PathfindingData data = new PathfindingData(new GridCoord(x, y), 0, 0);
                    pathfindingData[x + cellCountX * y] = data;
                }
            }

            this.grid = grid;
        }

        private PathfindingData GetPathfindingData(GridCoord coord)
        {
            // outside of the grid 
            if (coord.x >= cellCountX || coord.y >= cellCountY || coord.x < 0 || coord.y < 0)
                return null;

            int idx = coord.x + cellCountX * coord.y;

            if (idx >= 0 && idx < pathfindingData.Length)
                return pathfindingData[idx];

            return null;
        }

        /// <summary>
        /// Breadth first search with a maximum radius
        /// </summary>
        /// <param name="center">starting coordinate</param>
        /// <param name="radius">maxiumum radius</param>
        /// <returns>list of all visited cells</returns>
        public List<GridCoord> FloodFill(GridCubeCoord center, int radius)
        {
            List<GridCoord> cells = new List<GridCoord>();
            closedSet.Clear();

            //Get the center Tile, add it to the frontier and set its distance to the center to 0
            PathfindingData currentCell = GetPathfindingData(center);
            frontier.Enqueue(currentCell);
            closedSet.Add(currentCell);
            currentCell.distanceFromStart = 0;

            while (frontier.Count > 0)
            {
                currentCell = frontier.Dequeue();
                cells.Add(currentCell.coord);
                blockDirMask = GridCubeCoordBlockDirFlags.North;

                for (int i = 0; i < 6; i++)
                {
                    neighbourCoord = currentCell.coord + GridCubeCoord.neighborList[i];
                    PathfindingData neighbourCell = GetPathfindingData(neighbourCoord);

                    //If neighbour not exists or was already visited skip
                    if (grid.GetCell(currentCell.coord).BlockDirFlags.HasFlag(blockDirMask) ||
                        closedSet.Contains(neighbourCell) ||
                        grid.HasCell(neighbourCoord))
                    {
                        blockDirMask = blockDirMask.RotateRight(1);
                        continue;
                    }

                    neighbourCell.distanceFromStart = currentCell.distanceFromStart + 1;

                    closedSet.Add(neighbourCell);

                    if (neighbourCell.distanceFromStart <= radius)
                        frontier.Enqueue(neighbourCell);

                    blockDirMask = blockDirMask.RotateRight(1);
                }
            }
            return cells;
        }

        /// <summary>
        /// A*-Pathfinding
        /// </summary>
        /// <param name="startCoord">orgin of the resulting path</param>
        /// <param name="targetCoord">target for the pathfinding</param>
        /// <returns>List of GridCoords from start to target. null if no path was found</returns>
        public List<GridCoord> AStar(HexCell startCell, HexCell targetCell)
        {
            if (startCell == null || targetCell == null)
                return null;

            return AStar(startCell.coordinates, targetCell.coordinates);
        }

        /// <summary>
        /// A*-Pathfinding
        /// </summary>
        /// <param name="startCell">origin of the resulting path</param>
        /// <param name="targetCell">target for the pathfinding</param>
        /// <returns>List of HexCells from start to target or a nearest cell if target was unreachable.</returns>
        public List<GridCoord> AStar(GridCoord startCoord, GridCoord targetCoord)
        {
            openSet.Clear();
            closedSet.Clear();

            PathfindingData startCell = GetPathfindingData(startCoord);
            startCell.heuristicToTarget = GridCubeCoord.GetDistance(startCoord, targetCoord);
            openSet.Add(startCell);

            while (openSet.Count > 0)
            {
                PathfindingData currentCell = openSet.RemoveFirst();

                // return path when atrget cell is reached
                if (currentCell.coord == targetCoord)
                {
                    return RetracePath(startCell, currentCell);
                }

                closedSet.Add(currentCell);

                // get random variables and apply to blocking mask
                int indexOffset = Random.Range(0, 6);
                bool isLeftTurning = Random.value < 0.5 ? false : true;
                //int indexOffset = 0;
                //bool isLeftTurning = true;
                blockDirMask = GridCubeCoordBlockDirFlags.North.RotateRight(indexOffset);
                dir = (GridCubeCoordDirection)indexOffset;

                // expand to neighbours
                for (int i = 0; i < 6; i++)
                {
                    neighbourCoord = currentCell.coord + GridCubeCoord.neighborList[(int)dir];
                    dir = isLeftTurning ? dir.Previous() : dir.Next();
                    PathfindingData neighbourCell = GetPathfindingData(neighbourCoord);

                    // go to next neighbour if blocked or already closed or nonexistent or has unit
                    if (grid.GetCell(currentCell.coord).BlockDirFlags.HasFlag(blockDirMask) ||
                        closedSet.Contains(neighbourCell) ||
                        !grid.HasCell(neighbourCoord) ||
                        grid.runtimeUnits.HasCellUnit(neighbourCoord))
                    {
                        blockDirMask = isLeftTurning ? blockDirMask.RotateLeft(1) : blockDirMask.RotateRight(1);
                        continue;
                    }

                    newDistance = currentCell.distanceFromStart + 1;

                    // go to next neighbour if already visited with better route
                    if (openSet.Contains(neighbourCell) && neighbourCell.distanceFromStart <= newDistance)
                    {
                        blockDirMask = isLeftTurning ? blockDirMask.RotateLeft(1) : blockDirMask.RotateRight(1);
                        continue;
                    }

                    // set cell values
                    neighbourCell.pathPredecessor = currentCell;
                    neighbourCell.distanceFromStart = newDistance;
                    neighbourCell.heuristicToTarget = GridCubeCoord.GetDistance(neighbourCell.coord, targetCoord);

                    if (openSet.Contains(neighbourCell))
                        openSet.UpdateItem(neighbourCell);
                    else
                        openSet.Add(neighbourCell);

                    blockDirMask = isLeftTurning ? blockDirMask.RotateLeft(1) : blockDirMask.RotateRight(1);
                }
            }
            return RetracePath(startCell, closedSet.OrderBy(x => x.heuristicToTarget).First());
        }

        private List<GridCoord> RetracePath(PathfindingData startCell, PathfindingData endCell)
        {
            List<GridCoord> path = new List<GridCoord>();
            PathfindingData currentCell = endCell;

            while (currentCell != startCell)
            {
                path.Add(currentCell.coord);
                currentCell = currentCell.pathPredecessor;
            }
            path.Add(startCell.coord);
            path.Reverse();
            return path;
        }
    }
}