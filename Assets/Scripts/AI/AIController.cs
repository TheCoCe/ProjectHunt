using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace ProjectHunt
{
    // TODO: this class will be responsible to check all the different AIAbility objects and execute the best one
    [RequireComponent(typeof(Unit))]
    public class AIController : MonoBehaviour
    {
        public float discardThreshold = 0.01f;

        private Unit unit;

        void Start()
        {
            unit = GetComponent<Unit>();

#if DEBUG
            foreach (AIAbility ablt in unit.Abilities)
                Debug.Log(ablt);
#endif
        }

        public void PlanTurn()
        {
            if (unit.ActiveAbility && !unit.ActiveAbility.IsRunning)
            {
                foreach (Ability ablt in unit.Abilities)
                    (ablt as AIAbility)?.SelectTargets();

                AIAbility ability = Evaluate();
                if (ability)
                {
                    Debug.Log("Ability: " + ability.name + ", Utility: " + ability.Weight);
                    unit.SetActiveAbility(ability);
                    ability.SetRunning();
                }
            }
        }

        private AIAbility Evaluate()
        {
            IEnumerable<AIAbility> abilities = unit.Abilities.Cast<AIAbility>();

            foreach (var ablt in abilities)
            {
                ablt.ResetEvaluation();
            }

            abilities.OrderBy(a => a.Weight);

            // get total weight of abilities over the threshold
            float randomWeight = 0f;
            foreach (var ability in abilities)
            {
                randomWeight += ability.Weight > discardThreshold ? ability.Weight : 0f;
            }

            // early out if all ability weights are below the threshold
            if (randomWeight == 0f)
                return null;

            // weighted random selection
            float random = Random.value * randomWeight;
            float additiveWeight = 0f;
            foreach (var ability in abilities)
            {
                additiveWeight += ability.Weight;
                if (additiveWeight > random)
                    return ability;
            }

            Debug.Assert(false, "What the fuck, we should never reach this code!");
            return null;
        }
    }
}
