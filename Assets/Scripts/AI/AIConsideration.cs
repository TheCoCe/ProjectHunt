using UnityEngine;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "newAIConsideration", menuName = "ProjectHunt/AI Consideration", order = 0)]
    public class AIConsideration : ScriptableObject
    {
        public AnimationCurve baseEvaluationCurve;
        public AnimationCurve multEvaluationCurve;
        public AIParameter parameter;

        bool evaluatedBaseThisTurn = false;
        bool evaluatedMultThisTurn = false;

        [SerializeField] [HideInInspector] float baseValue = 0f;
        public float BaseValue
        {
            get
            {
                if (!evaluatedBaseThisTurn)
                {
                    baseValue = GetBaseValue();
                }
                return baseValue;
            }
        }

        [SerializeField] [HideInInspector] float multValue = 0f;
        public float MultValue
        {
            get
            {
                if (!evaluatedMultThisTurn)
                    multValue = GetMultValue();
                return multValue;
            }
        }

        public void Init(Unit unit, Ability ability)
        {
            parameter = Instantiate(parameter);
            parameter.Init(unit, ability);
        }

        public void ResetEvaluation()
        {
            evaluatedBaseThisTurn = false;
            evaluatedMultThisTurn = false;
        }

        private float GetBaseValue()
        {
            if (parameter != null && baseEvaluationCurve != null)
                return baseEvaluationCurve.Evaluate(parameter.GetValue());
            return 0f;
        }

        private float GetMultValue()
        {
            if (parameter != null && multEvaluationCurve != null)
                return multEvaluationCurve.Evaluate(parameter.GetValue());
            return 0f;
        }
    }
}