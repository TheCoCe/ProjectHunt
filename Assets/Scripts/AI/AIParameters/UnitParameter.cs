using UnityEngine;
using System;
using System.Reflection;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "new_unit_param", menuName = "ProjectHunt/Parameters/UnitParameter", order = 0)]
    public class UnitParameter : AIParameter
    {
        [SerializeField] private RuntimeUnits unitData;
        [SerializeField] private string unitName;
        [SerializeField] public string floatParameter;

        private Func<float> getter = null;

        private void InitGetter()
        {
            Unit unit = null;
            if (unitData.TryGetUnit(unitName, out unit))
            {
                PropertyInfo propertyInfo = null;
                if ((propertyInfo = typeof(Unit).GetProperty(floatParameter)) != null && propertyInfo.PropertyType == typeof(float))
                    getter = propertyInfo.GetMethod.CreateDelegate(typeof(Func<float>), unit) as Func<float>;
                else
                    Debug.LogError("UnitParameter (" + name + "): Invalid parameter name!");
            }
            else
                Debug.LogError("UnitParameter (" + name + "): Invalid unit name!");
        }

        public override float GetValue()
        {
            if (getter == null)
                InitGetter();

            return getter();
        }
    }
}