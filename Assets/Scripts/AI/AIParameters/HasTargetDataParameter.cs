using UnityEngine;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "new_has_target_data_parameter", menuName = "ProjectHunt/Parameters/HasTargetDataParameter", order = 0)]
    public class HasTargetDataParameter : AIParameter
    {
        Ability ability;

        public override void Init(Unit unit, Ability ability)
        {
            this.ability = ability;
        }

        public override float GetValue()
        {
            return ability.HasTargetData ? 1f : 0f;
        }
    }
}