using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "new_units_in_range_param", menuName = "ProjectHunt/Parameters/UnitsInRangeParameter", order = 0)]
    public class UnitsInRangeParameter : AIParameter
    {
        [SerializeField] private RuntimeUnits runtimeUnits;
        [SerializeField] private int range;
        [SerializeField] private UnitTags tags;
        private IEnumerable<Unit> query;
        private Unit myUnit;

        public override void Init(Unit unit, Ability ability)
        {
            myUnit = unit;
            query = runtimeUnits.CreateUnitQuery().Where(x =>
                (x.tags & tags) == tags &&
                GridCubeCoord.GetDistance(myUnit.Coordinate, x.Coordinate) <= range);
        }

        public override float GetValue()
        {
            return query.Count();
        }
    }
}