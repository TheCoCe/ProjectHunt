using UnityEngine;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "new_constant_param", menuName = "ProjectHunt/Parameter/ConstantParameter", order = 0)]
    public class ConstantParameter : AIParameter
    {
        [SerializeField] private float constant = 0;

        public override float GetValue()
        {
            return constant;
        }
    }
}