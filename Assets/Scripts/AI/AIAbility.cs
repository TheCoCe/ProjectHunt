using UnityEngine;
using System.Collections.Generic;
using System;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "new_ai_ability", menuName = "ProjectHunt/Abilities/AIAbility", order = 0)]
    public class AIAbility : Ability
    {
        [SerializeField] private AITargetSelector targetSelector;
        [SerializeField] private List<AIConsideration> considerations;

        [SerializeField][HideInInspector] float weight = 0f;
        public float Weight
        {
            get
            {
                if (!evaluatedThisTurn)
                    weight = Evaluate();
                return weight;
            }
        }

        private bool evaluatedThisTurn = false;

        public override void Init(Unit unit)
        {
            base.Init(unit);

            for (int i = 0; i < considerations.Count; i++)
            {
                considerations[i] = Instantiate(considerations[i]);
                considerations[i].Init(unit, this);
            }
        }

        public void ResetEvaluation()
        {
            evaluatedThisTurn = false;
            foreach (var consideration in considerations)
            {
                consideration.ResetEvaluation();
            }
        }

        private float Evaluate()
        {
            if (considerations.Count == 0)
                return 0f;

            float weight = 0f;
            float mult = 1f;
            foreach (var item in considerations)
            {
                weight += item.BaseValue;
                mult *= item.MultValue;
            }
            // normalize the sum
            weight /= considerations.Count;

            evaluatedThisTurn = true;
            return weight * mult;
        }

        public override void SelectTargets()
        {
            if (!IsRunning)
            {
                targetSelector.Initialize(unit);
                OnTargetData(targetSelector.GetTargetData());
                EndTargetSelection();
            }
        }

        public override void EndTargetSelection()
        {
            targetSelector.Deinitialize();
        }

        public void SetRunning()
        {
            IsRunning = true;
        }
    }
}