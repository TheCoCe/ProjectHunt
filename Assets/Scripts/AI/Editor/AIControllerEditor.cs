using UnityEngine;
using UnityEditor;
using ProjectHunt;

[CustomEditor(typeof(AIController))]
[CanEditMultipleObjects]
public class AIControllerEditor : Editor
{
    private SerializedProperty discardThreshold;
    private SerializedProperty abilitiesArray;
    private Editor abilityEditor;

    private GUIStyle abilityStyle;
    private Texture2D tex;

    private void OnEnable()
    {
        // get properties
        discardThreshold = serializedObject.FindProperty("discardThreshold");

        // setup styles
        abilityStyle = new GUIStyle();
        tex = new Texture2D(1, 1);
        tex.SetPixel(0, 0, new Color(.3f, .3f, .3f));
        tex.Apply();
        abilityStyle.normal.background = tex;
        abilityStyle.padding = new RectOffset(5, 6, 5, 5);
    }

    private void OnDisable()
    {
        Object.DestroyImmediate(abilityEditor);
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        // draw discard threshold as slider
        EditorGUILayout.Slider(discardThreshold, 0f, 1f);

        /*         // draw ability list field
                EditorGUILayout.PropertyField(abilitiesArray);

                if (abilitiesArray.isExpanded)
                {
                    for (int i = 0; i < abilitiesArray.arraySize; i++)
                    {
                        // begin vertical group for colored background and draw ability header
                        EditorGUILayout.Space();
                        EditorGUILayout.BeginVertical(abilityStyle);
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("Ability " + i, EditorStyles.boldLabel, GUILayout.Width(EditorGUIUtility.labelWidth));
                        EditorGUILayout.ObjectField(abilitiesArray.GetArrayElementAtIndex(i), GUIContent.none);
                        EditorGUILayout.EndHorizontal();
                        EditorGUI.indentLevel++;

                        // draw custom inspector for each ability
                        abilityEditor = Editor.CreateEditor(abilitiesArray.GetArrayElementAtIndex(i).objectReferenceValue);
                        abilityEditor.OnInspectorGUI();

                        EditorGUI.indentLevel--;
                        GUILayout.EndVertical();
                    }
                } */
        serializedObject.ApplyModifiedProperties();
    }
}