using UnityEngine;
using UnityEditor;
using ProjectHunt;

[CustomEditor(typeof(AIAbility))]
[CanEditMultipleObjects]
public class AIAbilityEditor : Editor
{
    private SerializedProperty targetSelector;
    private SerializedProperty actions;
    private SerializedProperty considerationsArray;
    private SerializedObject consideration;

    private void OnEnable()
    {
        targetSelector = serializedObject.FindProperty("targetSelector");
        actions = serializedObject.FindProperty("actions");
        considerationsArray = serializedObject.FindProperty("considerations");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        // draw target selctor field
        EditorGUILayout.PropertyField(targetSelector);

        // draw actions list field
        EditorGUILayout.PropertyField(actions, new GUIContent("List of Actions"));

        // draw considerations list field
        EditorGUILayout.PropertyField(considerationsArray, new GUIContent("List of Considerations"));

        if (considerationsArray.isExpanded)
        {
            // get and display properties for each consideration
            for (int i = 0; i < considerationsArray.arraySize; i++)
            {
                if (considerationsArray.GetArrayElementAtIndex(i).objectReferenceValue != null)
                {
                    // get consideration
                    consideration = new SerializedObject(considerationsArray.GetArrayElementAtIndex(i).objectReferenceValue);

                    // display reference to the consideration
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField("Consideration " + i, EditorStyles.boldLabel, GUILayout.Width(EditorGUIUtility.labelWidth - EditorGUI.indentLevel * 15f));
                    EditorGUILayout.ObjectField(considerationsArray.GetArrayElementAtIndex(i), GUIContent.none);
                    EditorGUILayout.EndHorizontal();

                    // draw consideration properties in one line (for saving space in the inspector)
                    // EditorGUILayout.BeginHorizontal();
                    // EditorGUILayout.ObjectField(consideration.FindProperty("parameter"), GUIContent.none);
                    // EditorGUILayout.CurveField(consideration.FindProperty("baseEvaluationCurve"), Color.green, Rect.zero, GUIContent.none);
                    // GUILayout.Label(consideration.FindProperty("baseValue").floatValue.ToString("f3"), GUILayout.MinWidth(30f));
                    // EditorGUILayout.CurveField(consideration.FindProperty("multEvaluationCurve"), Color.green, Rect.zero, GUIContent.none);
                    // GUILayout.Label(consideration.FindProperty("multValue").floatValue.ToString("f3"), GUILayout.MinWidth(30f));
                    // EditorGUILayout.EndHorizontal();

                    //draw consideration properties as block (looks more like standard Unity inspector)
                    EditorGUILayout.ObjectField(consideration.FindProperty("parameter"));
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.CurveField(consideration.FindProperty("baseEvaluationCurve"), Color.green, Rect.zero, GUILayout.MinWidth(260f));
                    GUILayout.FlexibleSpace();
                    GUILayout.Label("Base Value: " + consideration.FindProperty("baseValue").floatValue.ToString("f2"), GUILayout.MinWidth(30f));
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.CurveField(consideration.FindProperty("multEvaluationCurve"), Color.green, Rect.zero, GUILayout.MinWidth(260f));
                    GUILayout.FlexibleSpace();
                    GUILayout.Label("Multiplier Value: " + consideration.FindProperty("multValue").floatValue.ToString("f2"), GUILayout.MinWidth(30f));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.Space();

                    consideration.ApplyModifiedProperties();
                }
            }
        }

        // draw total weight
        GUILayout.Label("Total weight: " + serializedObject.FindProperty("weight").floatValue.ToString("f2"));

        serializedObject.ApplyModifiedProperties();
    }
}