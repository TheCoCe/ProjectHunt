using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "new_closest_in_range_target", menuName = "ProjectHunt/Target Selectors/ClosestInRangeTargetSelector", order = 0)]
    public class ClosestInRangeTargetSelector : AITargetSelector
    {
        [SerializeField] private RuntimeUnits runtimeUnits;

        [SerializeField] private UnitTags targetTags;

        public override AbilityTargetData GetTargetData()
        {
            IEnumerable<Unit> query = runtimeUnits.CreateUnitQuery().Where(x =>
                (x.tags & targetTags) == targetTags &&
                GridCubeCoord.GetDistance(unit.Coordinate, x.Coordinate) <= unit.AttackRange);

            if (query.Count() > 0)
            {
                UnitsAbilityTargetData targetData = new() { units = new() { query.First() } };
                return targetData;
            }
            return null;
        }
    }
}