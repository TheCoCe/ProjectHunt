using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace ProjectHunt
{
    [CreateAssetMenu(fileName = "new_closest_unit_path_target_selector", menuName = "ProjectHunt/Target Selectors/ClosestUnitPathTargetSelector", order = 0)]
    public class ClosestUnitPathTargetSelector : AITargetSelector
    {
        [SerializeField] private RuntimeUnits runtimeUnits;
        [SerializeField][Tooltip("Ignored if 0")] private int maxDetectionRange = 0;
        [SerializeField] private UnitTags filterTags;
        [SerializeField] private bool moveTowards = true;

        public override AbilityTargetData GetTargetData()
        {
            IEnumerable<Unit> query = runtimeUnits.CreateUnitQuery().Where(x =>
                (x.tags & filterTags) == filterTags &&
                (maxDetectionRange <= 0 || GridCubeCoord.GetDistance(unit.Coordinate, x.Coordinate) <= maxDetectionRange));

            if (query.Count() > 0)
            {
                // get movement direction
                query = query.OrderBy(x => GridCubeCoord.GetDistance(x.Coordinate, unit.Coordinate));
                Vector3 dir = moveTowards ? (Vector3)unit.Coordinate - query.First().Coordinate : query.First().Coordinate - (Vector3)unit.Coordinate;

                // pathfinding
                List<GridCoord> path = unit.ParentGrid.Pathfinding.AStar(unit.Coordinate, (GridCubeCoord)(unit.Coordinate - (Vector3.Normalize(dir) * unit.MovementRange * 2)));

                PathAbilityTargetData result = new() { path = new List<GridCoord>(path) };

                return result;
            }
            return null;
        }
    }
}