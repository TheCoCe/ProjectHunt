namespace ProjectHunt
{
    public abstract class AIParameter : UnityEngine.ScriptableObject
    {
        public abstract float GetValue();
        public virtual void Init(Unit unit, Ability ability) { }
    }
}
