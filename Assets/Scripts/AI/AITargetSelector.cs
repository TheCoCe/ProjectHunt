using UnityEngine;

namespace ProjectHunt
{
    public abstract class AITargetSelector : ScriptableObject
    {
        protected Unit unit;

        public virtual void Initialize(Unit unit)
        {
            this.unit = unit;
            Debug.Assert(unit != null, "Unit can't be null!");
        }

        public virtual void Deinitialize()
        {
            unit = null;
        }

        public abstract AbilityTargetData GetTargetData();
    }
}