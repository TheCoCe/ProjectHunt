using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public class UnitSpawner : MonoBehaviour
    {
        [SerializeField] private Unit playerUnit;
        [SerializeField] private Unit AIUnit;
        [SerializeField] private RuntimeUnits runtimeUnits;
        [SerializeField] private HexGrid grid;

        private void Start()
        {
            InputManager.Instance.RegisterCallback(InputContextID.Debugging, SpawnUnit);
        }

        private void OnDisable()
        {
            InputManager.Instance.UnregisterCallback(InputContextID.Debugging, SpawnUnit);
        }

        private MappedInput SpawnUnit(MappedInput input)
        {
            // Debug.Log("Try spawning Unit at " + input.clickedCell);

            if (Input.GetKey("left ctrl") && input.clickedCell && !runtimeUnits.HasCellUnit(input.clickedCell.coordinates))
            {
                Unit spawnedUnit = GameObject.Instantiate(playerUnit, (Vector3)input.clickedCell.coordinates, Quaternion.identity);
                spawnedUnit.Init();
                spawnedUnit.Coordinate = input.clickedCell.coordinates;
                spawnedUnit.ParentGrid = grid;
                input.clickedCell = null;
            }

            if (Input.GetKey("left shift") && input.clickedCell && !runtimeUnits.HasCellUnit(input.clickedCell.coordinates))
            {
                Unit spawnedUnit = GameObject.Instantiate(AIUnit, (Vector3)input.clickedCell.coordinates, Quaternion.identity);
                spawnedUnit.Init();
                spawnedUnit.Coordinate = input.clickedCell.coordinates;
                spawnedUnit.ParentGrid = grid;
                input.clickedCell = null;
            }
            return input;
        }
    }
}
