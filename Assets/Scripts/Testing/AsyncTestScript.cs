using UnityEngine;
using Cysharp.Threading.Tasks;
using System.Threading;

public class AsyncTestScript : MonoBehaviour
{
    [SerializeField] private float duration = 1f;
    [SerializeField] private float rotSpeed = 150f;

    CancellationTokenSource cts = new CancellationTokenSource();

    public async UniTask RotateObject(CancellationToken ct)
    {
        float curDur = 0f;
        while (curDur < duration)
        {
            transform.Rotate(new Vector3(1f, 1f) * Time.deltaTime * rotSpeed);
            curDur += Time.deltaTime;
            await UniTask.Yield(PlayerLoopTiming.Update, ct);
        }
    }

    /// <summary>
    /// This function is called when the MonoBehaviour will be destroyed.
    /// </summary>
    void OnDestroy()
    {
        cts.Cancel();
        cts.Dispose();
    }

    Rect windowRect = new Rect(100, 10, 100, 90);

    /// <summary>
    /// OnGUI is called for rendering and handling GUI events.
    /// This function can be called multiple times per frame (one call per event).
    /// </summary>
    void OnGUI()
    {
        windowRect = GUI.Window(0, windowRect, (int id) =>
        {
            if (GUI.Button(new Rect(10, 20, 80, 20), "Rotate"))
                RotateObject(cts.Token).Forget();

            if (GUI.Button(new Rect(10, 50, 80, 20), "Delete"))
            {
                this.gameObject.AddComponent<AsyncTestScript>();
                Destroy(this);
            }

            GUI.DragWindow(new Rect(0, 0, 1000, 20));
        }, "Async Test");
    }
}
