using UnityEngine;
using System;
using System.Collections.Generic;

namespace ProjectHunt
{
    public enum SpriteType
    {
        CellHighlight
    }

    [Serializable]
    public struct SpritePrefab
    {
        public SpriteType type;
        public HexSprite prefab;
        public int number;
    }

    public class SpriteManager : SingletonBehaviour<SpriteManager>
    {
        public SpritePrefab[] spritePrefabs;

        private List<HexSprite>[] instancedSprites;

        private void Awake()
        {
            instancedSprites = new List<HexSprite>[spritePrefabs.Length];
            for (int i = 0; i < spritePrefabs.Length; i++)
                instancedSprites[i] = new List<HexSprite>();

            GameObject tmpObj;
            HexSprite tmpSprite;
            for (int i = 0; i < spritePrefabs.Length; i++)
            {
                for (int j = 0; j < spritePrefabs[i].number; j++)
                {
                    tmpObj = Instantiate(spritePrefabs[i].prefab.gameObject, transform);
                    tmpSprite = tmpObj.GetComponent<HexSprite>();
                    instancedSprites[i].Add(tmpSprite);
                }
            }
        }

        /// <summary>
        /// Gets a number of unused sprites. Call Free() on each sprite or set its IsReserved to false, when the sprite isn't used anymore.
        /// </summary>
        /// <param name="type">Type of the requested sprites</param>
        /// <param name="number">Number of sprites that is requested</param>
        /// <returns></returns>
        public List<HexSprite> ReserveSprite(SpriteType type, int number)
        {
            List<HexSprite> sprites = new List<HexSprite>(number);

            // get inactive sprites
            foreach (HexSprite sprite in instancedSprites[(int)type])
            {
                if (!sprite.IsReserved)
                {
                    sprite.IsReserved = true;
                    sprites.Add(sprite);

                    if (sprites.Count == number)
                        return sprites;
                }
            }

            // create new sprites if necessary
            int diff = number - sprites.Count;
            for (int i = 0; i < diff; i++)
                sprites.Add(NewSprite(type));

            return sprites;
        }

        /// <summary>
        /// Gets an unused sprite. Call Free() on the sprite or set IsReserved to false, when the sprite isn't used anymore.
        /// </summary>
        /// <param name="type">Type of the requested sprite</param>
        /// <returns></returns>
        public HexSprite ReserveSprite(SpriteType type)
        {
            // look for inactive sprite
            foreach (HexSprite sprite in instancedSprites[(int)type])
            {
                if (!sprite.IsReserved)
                {
                    sprite.IsReserved = true;
                    return sprite;
                }
            }

            // create new sprite if all active
            return NewSprite(type);
        }

        /// <summary>
        /// Frees all sprites of given type. Handle with care, as this can effect multiple different scripts!
        /// </summary>
        /// <param name="type"></param>
        public void FreeType(SpriteType type)
        {
            foreach (HexSprite sprite in instancedSprites[(int)type])
                sprite.IsReserved = false;
        }

        /// <summary>
        /// Frees all reserved sprites. Handle with care, as this can effect multiple different scripts!
        /// </summary>
        public void FreeAll()
        {
            foreach (List<HexSprite> spriteList in instancedSprites)
                foreach (HexSprite sprite in spriteList)
                    sprite.IsReserved = false;
        }

        private HexSprite NewSprite(SpriteType type)
        {
            GameObject tmpObj = Instantiate(spritePrefabs[(int)type].prefab.gameObject, transform);
            HexSprite tmpSprite = tmpObj.GetComponent<HexSprite>();
            instancedSprites[(int)type].Add(tmpSprite);
            tmpSprite.IsReserved = true;
            return tmpSprite;
        }
    }
}