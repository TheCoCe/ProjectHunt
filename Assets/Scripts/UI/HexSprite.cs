using UnityEngine;
using UnityEngine.UI;
using ProjectHunt.HexTools;

namespace ProjectHunt
{
    [RequireComponent(typeof(Image))]
    [SerializeField]
    public class HexSprite : MonoBehaviour
    {
        private Image image;
        private Color defaultColor;

        private bool isReserved;
        public bool IsReserved
        {
            get { return isReserved; }
            set
            {
                if (value == false)
                    ActiveInHierarchy = false;
                isReserved = value;
            }
        }

        public bool ActiveInHierarchy
        {
            get { return gameObject.activeInHierarchy; }
            set { gameObject.SetActive(value); }
        }

        private void Awake()
        {
            image = GetComponent<Image>();
            defaultColor = image.color;

            ActiveInHierarchy = false;
        }

        public void Display(HexCell cell, Color color)
        {
            Vector3 spritePosition = (Vector3)cell.coordinates;
            spritePosition.y = cell.Elevation * HexMetrics.elevationAmplitude;
            transform.position = spritePosition;

            image.color = color;

            ActiveInHierarchy = true;
        }

        public void Free()
        {
            image.color = defaultColor;
            ActiveInHierarchy = false;
            IsReserved = false;
        }
    }
}