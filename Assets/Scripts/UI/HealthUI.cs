using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

namespace ProjectHunt
{
    public class HealthUI : MonoBehaviour
    {
        public Image healthBarFill;
        public TMP_Text healthText;
        public PlayerFaction playerFaction;
        private Unit currentUnit;

        void Start()
        {
            playerFaction.OnUnitSelectedEvent += OnUnitSelectionChanged;
            gameObject.SetActive(false);
        }

        public void OnUnitSelectionChanged(Unit unit)
        {
            if (unit == null)
            {
                if (currentUnit)
                    currentUnit.Health.OnHealthChanged -= UpdateHealthCallback;
                currentUnit = null;
                gameObject.SetActive(false);
            }
            else
            {
                if (currentUnit)
                    currentUnit.Health.OnHealthChanged -= UpdateHealthCallback;
                currentUnit = unit;
                currentUnit.Health.OnHealthChanged += UpdateHealthCallback;
                UpdateHealth(false);
                gameObject.SetActive(true);
            }
        }

        public void UpdateHealthCallback()
        {
            UpdateHealth();
        }

        public void UpdateHealth(bool tween = true)
        {
            if (!currentUnit)
                return;

            healthText.text = $"{currentUnit.Health.HealthValue}/{currentUnit.Health.MaxHealth}";
            if (currentUnit.Health.MaxHealth > 0)
            {
                float value = Mathf.Clamp01((float)currentUnit.Health.HealthValue / (float)currentUnit.Health.MaxHealth);
                if (tween)
                    DOTween.To(() => healthBarFill.fillAmount, x => healthBarFill.fillAmount = x, value, 1);
                else
                    healthBarFill.fillAmount = value;
            }
        }
    }
}
