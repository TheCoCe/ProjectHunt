using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ProjectHunt
{
    public class AbilityUI : MonoBehaviour
    {
        public Button buttonPrefab;
        public PlayerFaction playerFaction;

        private List<Button> buttonList;
        private Unit currentUnit = null;

        // Start is called before the first frame update
        void Start()
        {
            buttonList = new List<Button>();

            var childButtons = GetComponentsInChildren<Button>();
            foreach (var button in childButtons)
            {
                buttonList.Add(button);
                button.onClick.AddListener(() => OnAbilityButtonClicked(buttonList.Count - 1));
                button.gameObject.SetActive(false);
            }

            playerFaction.OnUnitSelectedEvent += OnUnitSelectedCallback;

            gameObject.SetActive(false);
        }

        void OnDisable()
        {
            //playerFaction.OnUnitSelectedEvent -= OnUnitSelectedCallback;
        }

        void OnDestroy()
        {
            playerFaction.OnUnitSelectedEvent -= OnUnitSelectedCallback;
        }

        void OnUnitSelectedCallback(Unit unit)
        {
            currentUnit = unit;
            if (currentUnit == null)
            {
                for (int i = 0; i < buttonList.Count; i++)
                {
                    if (buttonList[i].gameObject.activeSelf)
                        buttonList[i].gameObject.SetActive(false);
                    else
                        break;
                }

                gameObject.SetActive(false);
            }
            else
            {
                var abilities = currentUnit.Abilities;
                CheckButtonCount(abilities.Count);

                for (int i = 0; i < buttonList.Count; i++)
                {
                    if (i < abilities.Count)
                    {
                        buttonList[i].gameObject.SetActive(true);
                        // Set name and icon
                        var text = buttonList[i].GetComponentInChildren<TMP_Text>();
                        if (text)
                        {
                            text.text = abilities[i].name;
                        }
                    }
                    else if (buttonList[i].gameObject.activeSelf)
                    {
                        buttonList[i].gameObject.SetActive(false);
                    }
                    else
                    {
                        break;
                    }
                }

                gameObject.SetActive(true);
            }
        }

        void CheckButtonCount(int requiredCount)
        {
            int diff = requiredCount - buttonList.Count;
            for (int i = 0; i < diff; i++)
            {
                var button = Instantiate(buttonPrefab, this.transform);
                buttonList.Add(button);
                button.onClick.AddListener(() => OnAbilityButtonClicked(buttonList.Count - 1));
            }
        }

        void OnAbilityButtonClicked(int buttonIndex)
        {
            Debug.Assert(currentUnit != null, "You shouldn't be able to see or click any buttons when the currentUnit is null!");

            if (currentUnit.SetActiveAbilityIdx(buttonIndex))
            {
                Debug.Log("Ability selected");
            }
            else
            {
                Debug.Log("Ability selection failed");
            }
        }
    }
}
