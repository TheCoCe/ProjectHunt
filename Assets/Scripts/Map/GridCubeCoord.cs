﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    public enum GridCubeCoordDirection { North = 0, Northeast = 1, Southeast = 2, South = 3, Southwest = 4, Northwest = 5 };

    [Flags]
    [Serializable]
    public enum GridCubeCoordBlockDirFlags : uint
    {
        None = 0,
        North = 1 << 0,
        Northeast = 1 << 1,
        Southeast = 1 << 2,
        South = 1 << 3,
        Southwest = 1 << 4,
        Northwest = 1 << 5
    };

    /// <summary>
    /// Cube coords as defined here: http://redblobgames.com/grids/hexagons
    /// 
    /// Note that X, Y, Z have NO correlation to vector x, y, z
    /// </summary>
    public struct GridCubeCoord : IEquatable<GridCubeCoord>
    {
        #region Data

        public static readonly GridCubeCoord invalid = new GridCubeCoord(-100, -100, -100);

        const float width = 2f;
        static readonly float height = Mathf.Sqrt(3) / 2f * width;
        static readonly float size = width * .5f;
        public static float Size
        {
            get
            {
                return size;
            }
        }

        public static readonly GridCubeCoord[] neighborList = new GridCubeCoord[]
        {
            new GridCubeCoord(0, -1, 1), // North
			new GridCubeCoord(1, -1, 0),
            new GridCubeCoord(1, 0, -1),
            new GridCubeCoord(0, 1, -1),
            new GridCubeCoord(-1, 1, 0),
            new GridCubeCoord(-1, 0, 1),
        };

        public static readonly Vector3[] cornerList = new Vector3[]
        {
            new Vector3(width/4, 0, height/2),
            new Vector3(width/2, 0, 0),
            new Vector3(width/4, 0, -height/2),
            new Vector3(-width/4, 0, -height/2),
            new Vector3(-width/2, 0, 0),
            new Vector3(-width/4, 0, height/2), // North
        };

        public readonly int x, y, z;

        public int magnitude
        {
            get
            {
                return Math.Max(Math.Abs(x), Math.Max(Math.Abs(y), Math.Abs(z)));
            }
        }

        #endregion

        #region Init

        public GridCubeCoord(
          int x,
          int y,
          int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        #endregion

        #region Operators
        public static explicit operator GridCubeCoord(
         Vector3 position)
        {
            // Axial 
            float q = position.x * 2 / 3 / Size;
            float r = (-position.x / 3 + Mathf.Sqrt(3) / 3 * position.z) / Size;

            // Cube
            float x = q;
            float z = r;
            float y = -x - z;
            return RoundCubeCoordinate(x, y, z).coord;
        }

        public static implicit operator GridCubeCoord(
          GridCoord coord)
        {
            float x = coord.x;
            float z = coord.y - (coord.x - (coord.x & 1)) / 2;
            float y = -x - z;
            return RoundCubeCoordinate(x, y, z).coord;
        }

        public static GridCubeCoord operator +(
          GridCubeCoord a,
          GridCubeCoord b)
        {
            return new GridCubeCoord(a.x + b.x, a.y + b.y, a.z + b.z);
        }

        public static GridCubeCoord operator -(
          GridCubeCoord a,
          GridCubeCoord b)
        {
            return new GridCubeCoord(a.x - b.x, a.y - b.y, a.z - b.z);
        }

        public static GridCubeCoord operator *(
          GridCubeCoord a,
          int s)
        {
            return new GridCubeCoord(a.x * s, a.y * s, a.z * s);
        }

        public static bool operator ==(
          GridCubeCoord a,
          GridCubeCoord b)
        {
            return a.x == b.x && a.y == b.y && a.z == b.z;
        }

        public static bool operator !=(
          GridCubeCoord a,
          GridCubeCoord b)
        {
            return a.x != b.x || a.y != b.y || a.z != b.z;
        }


        #endregion

        #region Write

        /// <summary>
        /// Returns coord and sqrMag of the round
        /// </summary>
        public static ValueTuple<GridCubeCoord, float> Lerp(
          GridCubeCoord from,
          GridCubeCoord to,
          float percent)
        {
            Debug.Assert(float.IsNaN(percent) == false);

            return RoundCubeCoordinate(Mathf.Lerp(from.x, to.x, percent),
                        Mathf.Lerp(from.y, to.y, percent),
                        Mathf.Lerp(from.z, to.z, percent));
        }

        /// <summary>
        /// Returns coord and sqrMag of the round
        /// </summary>
        public static (GridCubeCoord coord, float roundSqrMag) RoundCubeCoordinate(
          float x,
          float y,
          float z)
        {
            Debug.Assert(float.IsNaN(x) == false);
            Debug.Assert(float.IsNaN(y) == false);
            Debug.Assert(float.IsNaN(z) == false);

            int rx = Mathf.RoundToInt(x);
            int ry = Mathf.RoundToInt(y);
            int rz = Mathf.RoundToInt(z);

            float x_diff = Mathf.Abs(rx - x);
            float y_diff = Mathf.Abs(ry - y);
            float z_diff = Mathf.Abs(rz - z);

            if (x_diff > y_diff && x_diff > z_diff)
            {
                rx = -ry - rz;
            }
            else if (y_diff > z_diff)
            {
                ry = -rx - rz;
            }
            else
            {
                Debug.Assert(rx < 1000 && rx > -1000);
                Debug.Assert(ry < 1000 && ry > -1009);

                rz = -rx - ry;
            }

            float sqrMagOfRound = x_diff * x_diff + y_diff * y_diff + z_diff * z_diff;

            return ValueTuple.Create(new GridCubeCoord(rx, ry, rz), sqrMagOfRound);
        }

        #endregion

        #region Read

        public static int GetDistance(
          GridCubeCoord a, GridCubeCoord b)
        {
            return (Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y) + Math.Abs(a.z - b.z)) / 2;
        }

        public static List<GridCubeCoord> GetTilesInRadius(GridCubeCoord center, int radius)
        {
            List<GridCubeCoord> list = new List<GridCubeCoord>();

            for (int x = -radius; x <= radius; x++)
            {
                for (int y = Mathf.Max(-radius, -x - radius); y <= Mathf.Min(radius, -x + radius); y++)
                {
                    int z = -x - y;
                    list.Add(new GridCubeCoord(x, y, z) + center);
                }
            }
            return list;
        }

        public static List<GridCubeCoord> GetTilesInRing(GridCubeCoord center, int radius)
        {
            if (radius == 0)
                return null;
            List<GridCubeCoord> list = new List<GridCubeCoord>();
            var cube = center + (neighborList[4] * radius);

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < radius; j++)
                {
                    list.Add(cube);
                    cube = cube + neighborList[i];
                }
            }

            return list;
        }

        public static bool GetNeighbourDirection(GridCubeCoord start, GridCubeCoord neighbour, out GridCubeCoordDirection dir)
        {
            //TODO: schneller machen!
            dir = GridCubeCoordDirection.North;
            if (start + neighborList[0] == neighbour)
                dir = GridCubeCoordDirection.North;
            else if (start + neighborList[1] == neighbour)
                dir = GridCubeCoordDirection.Northeast;
            else if (start + neighborList[2] == neighbour)
                dir = GridCubeCoordDirection.Southeast;
            else if (start + neighborList[3] == neighbour)
                dir = GridCubeCoordDirection.South;
            else if (start + neighborList[4] == neighbour)
                dir = GridCubeCoordDirection.Southwest;
            else if (start + neighborList[5] == neighbour)
                dir = GridCubeCoordDirection.Northwest;
            else
                return false;
            return true;
        }

        /// <summary>
        /// Rotates a GridCubeCoord around (0, 0, 0)
        /// </summary>
        public static GridCubeCoord GetRotatedCoord(GridCubeCoord coord, int rotation, bool left)
        {
            var (x, y, z) = coord;
            for (int i = 0; i < rotation; i++)
            {
                (x, y, z) = left ? (-z, -x, -y) : (-y, -z, -x);
            }
            return new GridCubeCoord(x, y, z);
        }

        /// <summary>
        /// Rotates a GridCubeCoord around center
        /// </summary>
        public static GridCubeCoord GetRotatedCoord(GridCubeCoord coord, GridCubeCoord center, int rotation, bool left)
        {
            GridCubeCoord relativeCoord = coord - center;
            var (x, y, z) = relativeCoord;
            for (int i = 0; i < rotation; i++)
            {
                (x, y, z) = left ? (-z, -x, -y) : (-y, -z, -x);
            }
            return new GridCubeCoord(x, y, z) + center;
        }
        #endregion

        #region Class stuff

        public bool Equals(GridCubeCoord other)
        {
            return x == other.x && y == other.y && z == other.z;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return x * 3 + y * 113 + z * 977;
        }

        public override string ToString()
        {
            GridCoord gridCoord = this;
            return $"{x}, {y}, {z} ({gridCoord.x}, {gridCoord.y})";
            //return "Cube coord " + x + ", " + y + ", " + z + "(" + gridCoord.x + ", " + gridCoord.y + ")";
        }

        public void Deconstruct(out int x, out int y, out int z)
        {
            x = this.x;
            y = this.y;
            z = this.z;
        }

        #endregion
    }

    public static class GridCubeCoordDirectionExtension
    {
        public static GridCubeCoordDirection Opposite(this GridCubeCoordDirection direction)
        {
            return (int)direction < 3 ? (direction + 3) : (direction - 3);
        }

        public static GridCubeCoordDirection Previous(this GridCubeCoordDirection direction)
        {
            return direction == GridCubeCoordDirection.North ? GridCubeCoordDirection.Northwest : (direction - 1);
        }

        public static GridCubeCoordDirection Next(this GridCubeCoordDirection direction)
        {
            return direction == GridCubeCoordDirection.Northwest ? GridCubeCoordDirection.North : (direction + 1);
        }
    }

    public static class GridCubeCoordBlockDirFlagsExtension
    {
        public static GridCubeCoordBlockDirFlags RotateRight(this GridCubeCoordBlockDirFlags value, int count)
        {
            return (GridCubeCoordBlockDirFlags)((((uint)value << count) | ((uint)value >> (6 - count))) & 63);
        }

        public static GridCubeCoordBlockDirFlags RotateLeft(this GridCubeCoordBlockDirFlags value, int count)
        {
            return (GridCubeCoordBlockDirFlags)((((uint)value >> count) | ((uint)value << (6 - count))) & 63);
        }

        public static GridCubeCoordBlockDirFlags Set(this GridCubeCoordBlockDirFlags value, GridCubeCoordBlockDirFlags flag)
        {
            return value | flag;
        }

        public static GridCubeCoordBlockDirFlags Unset(this GridCubeCoordBlockDirFlags value, GridCubeCoordBlockDirFlags flag)
        {
            return value & ~flag;
        }

        public static GridCubeCoordBlockDirFlags SetFlagForDirection(this GridCubeCoordBlockDirFlags value, GridCubeCoordDirection dir)
        {
            return (GridCubeCoordBlockDirFlags)((int)value | (1 << (int)dir));
        }

        public static GridCubeCoordBlockDirFlags UnsetFlagForDirection(this GridCubeCoordBlockDirFlags value, GridCubeCoordDirection dir)
        {
            return (GridCubeCoordBlockDirFlags)((int)value & ~(1 << (int)dir));
        }
    }
}
