﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt
{
    /// <summary>
    /// Flat top even q vertical layout.  
    /// http://www.redblobgames.com/grids/hexagons
    /// y is opposite from what's shown in pics
    /// </summary>
    [System.Serializable]
    public struct GridCoord : IComparable<GridCoord>, IEquatable<GridCoord>
    {
        #region Data

        public int x, y;

        #endregion

        #region Init

        public GridCoord(
          int x,
          int y)
        {
            this.x = x;
            this.y = y;
        }

        #endregion

        #region Operators

        public static bool operator ==(
          GridCoord a,
          GridCoord b)
        {
            return a.x == b.x && a.y == b.y;
        }

        public static bool operator !=(
          GridCoord a,
          GridCoord b)
        {
            return a.x != b.x || a.y != b.y;
        }

        public static explicit operator GridCoord(
          Vector3 position)
        {
            return (GridCoord)(GridCubeCoord)position;
        }

        public static implicit operator GridCoord(
          GridCubeCoord cubeCoord)
        {
            return new GridCoord(cubeCoord.x, cubeCoord.z + (cubeCoord.x - (cubeCoord.x & 1)) / 2);
        }

        public static implicit operator Vector3(
          GridCoord coord)
        {
            float x = GridCubeCoord.Size * 3 / 2 * coord.x;
            float z = GridCubeCoord.Size * Mathf.Sqrt(3) * (coord.y + .5f * (coord.x & 1));
            return new Vector3(x, 0, z);
        }

        #endregion

        #region Read
        /// <summary>
        /// Includes center.
        /// </summary>
        public static IEnumerable<GridCoord> InRadius(
          GridCubeCoord centerCubeCoord,
          int radius)
        {
            for (int dx = -radius; dx <= radius; dx++)
            {
                for (int dy = Math.Max(-radius, -dx - radius);
                  dy <= Math.Min(radius, -dx + radius);
                  dy++)
                {
                    int dz = -dx - dy;
                    GridCubeCoord cubeCoord = centerCubeCoord + new GridCubeCoord(dx, dy, dz);
                    GridCoord gridCoord = cubeCoord;
                    yield return gridCoord;
                }
            }
        }

        public static IEnumerable<GridCoord> GetDelta(
          GridCubeCoord previousCubeCenter,
          GridCubeCoord centerCubeCoord,
          int radius)
        {
            for (int dx = -radius; dx <= radius; dx++)
            {
                for (int dy = Math.Max(-radius, -dx - radius);
                  dy <= Math.Min(radius, -dx + radius);
                  dy++)
                {
                    int dz = -dx - dy;
                    GridCubeCoord step = new GridCubeCoord(dx, dy, dz);
                    GridCubeCoord cubeCoord = previousCubeCenter + step;
                    GridCubeCoord deltaFromPrevious = centerCubeCoord - cubeCoord;
                    if (deltaFromPrevious.magnitude > radius)
                    {
                        GridCoord gridCoord = cubeCoord;
                        yield return gridCoord;
                    }
                }
            }
        }

        public int CompareTo(
          GridCoord other)
        {
            int me = x + y;
            int you = other.x + other.y;
            return me > you ? 1 : me == you ? 0 : -1;
        }

        public static void IdentifyNeighbors(
          Vector3 position,
          out GridCoord a,
          out GridCoord b,
          out GridCoord c)
        {
            // Axial 
            float q = position.x * 2 / 3 / GridCubeCoord.Size;
            float r = (-position.x / 3 + Mathf.Sqrt(3) / 3 * position.z) / GridCubeCoord.Size;

            // Cube
            float x = q;
            float z = r;
            float y = -x - z;

            int rx = Mathf.RoundToInt(x);
            int ry = Mathf.RoundToInt(y);
            int rz = Mathf.RoundToInt(z);

            a = new GridCubeCoord(-ry - rz, ry, rz);
            b = new GridCubeCoord(rx, -rx - rz, rz);
            c = new GridCubeCoord(rx, ry, -rx - ry);
        }

        public static int ToIndex(GridCoord coord, int sizeX, int sizeY)
        {
            // outside of the grid 
            if (coord.x >= sizeX || coord.y >= sizeY || coord.x < 0 || coord.y < 0)
                return -1;

            int idx = coord.x + sizeX * coord.y;

            if (idx >= 0 && idx < sizeX * sizeY)
                return idx;

            return -1;
        }

        public static GridCoord FromIndex(int idx, int sizeX)
        {
            return new GridCoord(idx % sizeX, idx / sizeX);
        }

        #endregion

        #region Class stuff

        public bool Equals(
          GridCoord other)
        {
            return x == other.x && y == other.y;
        }

        public override bool Equals(
          object obj)
        {
            if (obj is GridCoord)
            {
                return this == (GridCoord)obj;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return x * 3 + y * 977;
        }

        public override string ToString()
        {
            return $"({x}, {y})";
        }

        public void Deconstruct(out int x, out int y)
        {
            x = this.x;
            y = this.y;
        }

        #endregion
    }
}
