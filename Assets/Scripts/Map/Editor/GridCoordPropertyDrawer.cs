using UnityEngine;
using UnityEditor;
using ProjectHunt;

[CustomPropertyDrawer(typeof(GridCoord))]
public class GridCoordPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        GridCoord coordinates = new GridCoord(
            property.FindPropertyRelative("x").intValue,
            property.FindPropertyRelative("y").intValue
        );

        GUI.Label(position, coordinates.ToString());
    }
}