using UnityEngine;
using UnityEditor;
using ProjectHunt;
using ProjectHunt.HexTools;

[CustomEditor(typeof(HexGridPlaceable))]
public class HexGridPlaceableInspector : Editor
{
    HexGridPlaceable placeable;
    HexGrid grid;

    static float flagClickAreaOffset = 0.7f;
    static bool editing = false;

    void OnEnable()
    {
        placeable = target as HexGridPlaceable;
        grid = FindObjectOfType<HexGrid>();
        if (!grid)
        {
            Debug.LogWarning("No Grid found!");
        }
    }

    void OnDisable()
    {
        Tools.hidden = false;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (placeable.placeableDefinition)
        {
            if (editing && GUILayout.Button("Stop editing"))
            {
                editing = false;
                SceneView.lastActiveSceneView.Repaint();
                AssetDatabase.SaveAssetIfDirty(placeable.placeableDefinition);
            }
            else if (!editing && GUILayout.Button("Start editing"))
            {
                editing = true;
                SceneView.lastActiveSceneView.Repaint();
            }
        }
    }

    void OnSceneGUI()
    {
        placeable.transform.position = (GridCoord)placeable.transform.position + new Vector3(0f, placeable.transform.position.y, 0f);
        DrawPlacableCoordDefinitionHandles();

        if (Tools.current == Tool.Rotate)
        {
            Tools.hidden = true;
            Handles.color = Color.green;
            Quaternion rotation = Handles.Disc(placeable.transform.rotation, placeable.transform.position, Vector3.up, HandleUtility.GetHandleSize(placeable.transform.position), false, 60);
            placeable.transform.rotation = rotation;

            Vector3 euler = placeable.transform.rotation.eulerAngles;
            euler.y = ((int)(euler.y / 60f) * 60f) % 360f;
            placeable.transform.rotation = Quaternion.Euler(euler);
        }
        else
            Tools.hidden = false;
    }

    GridCubeCoordBlockDirFlags ToggleFlagForDirection(GridCubeCoordBlockDirFlags def, GridCubeCoordDirection dir)
    {
        return (GridCubeCoordBlockDirFlags)((int)def ^ (1 << (int)dir));
    }

    GridCubeCoordBlockDirFlags SetAllFlagsDirsBlocked(GridCubeCoordBlockDirFlags def)
    {
        return def | (GridCubeCoordBlockDirFlags.North | GridCubeCoordBlockDirFlags.Northeast | GridCubeCoordBlockDirFlags.Southeast | GridCubeCoordBlockDirFlags.South | GridCubeCoordBlockDirFlags.Southwest | GridCubeCoordBlockDirFlags.Northwest);
    }

    void DrawPlacableCoordDefinitionHandles()
    {
        if (!placeable.placeableDefinition)
            return;

        int rot = Mathf.RoundToInt(placeable.transform.rotation.eulerAngles.y / 60f);
        bool left = rot < 0;
        rot = Mathf.Abs(rot) % 6;
        PlaceableCoordDefinitionDictionary dict = RotatePlaceableDefinitions(placeable.placeableDefinition.blockCoords, rot, left);

        GridCubeCoord coord = (GridCubeCoord)placeable.transform.position;

        foreach (var key in dict.Keys)
        {
            GridCubeCoord targetCoord = coord + key;
            Vector3 handlePos = (GridCoord)targetCoord;

            Handles.color = Color.red;
            DrawHexWithBlockedFlags(handlePos, dict[key], 3f, 0.9f);
        }

        if (editing)
        {
            Event e = Event.current;
            if (e.control || e.shift)
            {
                HandleUtility.AddDefaultControl(0);
            }

            if (e.type == EventType.MouseDown && e.button == 0 && e.control)
            {
                Vector3 hit;
                if (RaycastHexGrid(out hit))
                {
                    GridCubeCoord raycastCoord = (GridCoord)hit;
                    GridCubeCoord relativeCoord = GridCubeCoord.GetRotatedCoord(raycastCoord - coord, rot, !left);
                    if (Vector3.Distance((GridCoord)raycastCoord, hit) > flagClickAreaOffset)
                    {
                        GridCubeCoordDirection dir = FindClosestEdgeDirection(hit);
                        for (int i = 0; i < rot; i++)
                            dir = !left ? dir.Previous() : dir.Next();

                        if (placeable.placeableDefinition.blockCoords.ContainsKey(relativeCoord))
                        {
                            Undo.RecordObject(placeable.placeableDefinition, "Changed blocked flags");
                            EditorUtility.SetDirty(placeable.placeableDefinition);
                            placeable.placeableDefinition.blockCoords[relativeCoord] = ToggleFlagForDirection(placeable.placeableDefinition.blockCoords[relativeCoord], dir);
                        }
                    }
                    else
                    {
                        if (placeable.placeableDefinition.blockCoords.ContainsKey(relativeCoord))
                        {
                            Undo.RecordObject(placeable.placeableDefinition, "Changed blocked flags");
                            EditorUtility.SetDirty(placeable.placeableDefinition);
                            placeable.placeableDefinition.blockCoords.Remove(relativeCoord);
                        }
                        else
                        {
                            Undo.RecordObject(placeable.placeableDefinition, "Changed blocked flags");
                            EditorUtility.SetDirty(placeable.placeableDefinition);
                            placeable.placeableDefinition.blockCoords.Add(relativeCoord, 0);
                            if (e.shift)
                            {
                                placeable.placeableDefinition.blockCoords[relativeCoord] = SetAllFlagsDirsBlocked(placeable.placeableDefinition.blockCoords[relativeCoord]);
                            }
                        }
                    }
                }
                e.Use();
            }
        }
    }

    bool RaycastHexGrid(out Vector3 hitLocation)
    {
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        hitLocation = Vector3.zero;
        Plane groundPlane = new Plane(Vector3.zero, Vector3.forward, Vector3.right);

        float enter;
        if (groundPlane.Raycast(ray, out enter))
        {
            hitLocation = ray.GetPoint(enter);
            return true;
        }

        return false;
    }

    GridCubeCoordDirection FindClosestEdgeDirection(Vector3 position)
    {
        GridCubeCoord coord = (GridCoord)position;
        float distance = float.MaxValue;

        int i = 0, closestIndex = 0;
        for (; i < GridCubeCoord.neighborList.Length; i++)
        {
            Vector3 neighborPosition = (GridCoord)(coord + GridCubeCoord.neighborList[i]);
            float tempDist = Vector3.Distance(neighborPosition, position);
            if (tempDist < distance)
            {
                distance = tempDist;
                closestIndex = i;
            }
        }

        return (GridCubeCoordDirection)closestIndex;
    }

    PlaceableCoordDefinitionDictionary RotatePlaceableDefinitions(PlaceableCoordDefinitionDictionary dict, int rotation, bool left)
    {
        if (rotation == 0)
            return dict;
        rotation = Mathf.Abs(rotation) % 6;

        PlaceableCoordDefinitionDictionary newDict = new PlaceableCoordDefinitionDictionary();
        foreach (var item in dict)
        {
            GridCubeCoord rotatedCoord = GridCubeCoord.GetRotatedCoord(item.Key, rotation, left);
            GridCubeCoordBlockDirFlags flags = left ? item.Value.RotateLeft(rotation) : item.Value.RotateRight(rotation);
            newDict[rotatedCoord] = flags;
        }

        return newDict;
    }

    static void DrawHex(Vector3 position, float thickness)
    {
        for (int i = 0, j = 0; i < 6; i++, j += 2)
        {
            Handles.DrawLine(GridCubeCoord.cornerList[i] + position, GridCubeCoord.cornerList[(i + 1) % 6] + position, thickness);
        }
    }

    static void DrawHex(Vector3 position, float thickness, float sizeModifier)
    {
        for (int i = 0, j = 0; i < 6; i++, j += 2)
        {
            Handles.DrawLine(GridCubeCoord.cornerList[i] * sizeModifier + position, GridCubeCoord.cornerList[(i + 1) % 6] * sizeModifier + position, thickness);
        }
    }

    static void DrawHexWithBlockedFlags(Vector3 position, GridCubeCoordBlockDirFlags flags, float thickness, float sizeModifier)
    {
        for (int i = 0, j = 0; i < 6; i++, j += 2)
        {
            Handles.color = ((int)flags & 1 << i) == 0 ? Color.green : Color.red;
            Handles.DrawLine(GridCubeCoord.cornerList[i - 1 < 0 ? 5 : i - 1] * sizeModifier + position, GridCubeCoord.cornerList[i] * sizeModifier + position, thickness);
        }
    }
}