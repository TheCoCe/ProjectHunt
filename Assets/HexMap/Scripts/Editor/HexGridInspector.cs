using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using ProjectHunt;
using ProjectHunt.HexTools;

[CustomEditor(typeof(HexGrid))]
class HexMapCustomEditor : Editor
{
    #region DATA
    private int brushSize = 1;
    private int loadSaveMapSelection = 0;

    #region MODE
    enum HexEditorMode
    {
        None = -1,
        Height = 0,
        Texture = 1,
    }
    private static string[] mode_names = { "Height", "Texture" };
    private HexEditorMode mode_selected = HexEditorMode.None;
    #endregion

    #region HEIGHT
    enum HeightMode
    {
        Add = 0,
        Subtract = 1,
        Set = 2,
        Smooth = 3,
        Flatten = 4,
    }
    private static string[] heightEditingModes = { "Add", "Subtract", "Set", "Smooth", "Flatten" };
    private HeightMode heightMode = HeightMode.Add;
    private int height = 0;
    #endregion

    #region TERRAINTYPE
    private static string[] terrainTypes = { "Grass", "Stone", "Sand", "Mud", "Snow" };
    private int terrainTypeIndex = 0;
    #endregion

    public bool visualizeBlockFlags = false;

    #region RUNTIME
    private HexGrid grid;

    private bool editing = false;
    private int smoothTargetHeight = 0;
    private int flattenTargetHeight = 0;

    static HashSet<(int x, int y)> editedChunkCoordList = new HashSet<(int x, int y)>();
    static List<HexCell> cellsToEdit = new List<HexCell>();
    private HexCell currentCell = null;
    #endregion

    #region UNDOSTRINGS
    const string UNDO_ELEVATION = "Elevation change";
    const string UNDO_COLOR = "Color change";
    #endregion
    #endregion

    void OnEnable()
    {
        grid = target as HexGrid;

        Tools.hidden = true;

        Undo.undoRedoPerformed += UndoRedoPerformed;

        brushSize = EditorPrefs.GetInt("HEXMAPEDITOR_brushSize", 1);
        mode_selected = (HexEditorMode)EditorPrefs.GetInt("HEXMAPEDITOR_mode_selected", -1);
        heightMode = (HeightMode)EditorPrefs.GetInt("HEXMAPEDITOR_heightMode_selected", 0);
        terrainTypeIndex = EditorPrefs.GetInt("HEXMAPEDITOR_terrainTypeIndex", 0);
        loadSaveMapSelection = EditorPrefs.GetInt("HEXMAPEDITOR_loadSaveMapSelection", 0);
    }

    void OnDisable()
    {
        Tools.hidden = false;

        EditorPrefs.SetInt("HEXMAPEDITOR_brushSize", brushSize);
        EditorPrefs.SetInt("HEXMAPEDITOR_mode_selected", (int)mode_selected);
        EditorPrefs.SetInt("HEXMAPEDITOR_heightMode_selected", (int)heightMode);
        EditorPrefs.SetInt("HEXMAPEDITOR_terrainTypeIndex", terrainTypeIndex);
        EditorPrefs.SetInt("HEXMAPEDITOR_loadSaveMapSelection", loadSaveMapSelection);

        Undo.undoRedoPerformed -= UndoRedoPerformed;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (grid == null)
            return;

        VisualizeBlockFlagsGUI();
        GUILayout.Space(10);
        EditorGUILayout.LabelField("Hex Map Editor");

        SaveLoadMapGUI();
        CreateMapGUI();

        EditorGUILayout.LabelField("Tools");

        brushSize = EditorGUILayout.IntSlider("Brush size", brushSize, 0, 4);

        MapEditModeGUI();
    }

    public void OnSceneGUI()
    {
        if (mode_selected != HexEditorMode.None)
        {
            Vector3 center;
            if (!RaycastHexMap(out center))
            {
                editing = false;
                return;
            }

            CollectCells(center);

            DrawHandles(center);

            // Disable object selection
            HandleUtility.AddDefaultControl(0);

            if (Event.current.button == 0)
            {
                switch (Event.current.type)
                {
                    case EventType.MouseDown:
                        editing = true;
                        Event.current.Use();
                        BeginEditCells();
                        break;
                    case EventType.MouseUp:
                        editing = false;
                        Event.current.Use();
                        break;
                }
            }

            if (editing && Event.current.type == EventType.Repaint)
            {
                EditCells();

            }

            if (visualizeBlockFlags && Event.current.type == EventType.Repaint)
                DrawBlockFlags();
        }
    }

    void EditCells()
    {
        editedChunkCoordList.Clear();

        PrepareEditCells();

        foreach (var cell in cellsToEdit)
        {
            EditCell(cell);
        }

        UpdateChunks();
    }

    void EditCell(HexCell cell)
    {
        if (cell)
        {
            (int x, int y) chunkCoords = (cell.coordinates.x / HexMetrics.chunkSizeX, cell.coordinates.y / HexMetrics.chunkSizeZ);
            if (!editedChunkCoordList.Contains(chunkCoords))
            {
                editedChunkCoordList.Add(chunkCoords);
            }

            GridCoord offsetCoords = cell.coordinates;
            // we are at a chunkborder
            if (offsetCoords.x % HexMetrics.chunkSizeX == 0 ||
                offsetCoords.x % HexMetrics.chunkSizeX == HexMetrics.chunkSizeX - 1 ||
                offsetCoords.y % HexMetrics.chunkSizeZ == 0 ||
                offsetCoords.y % HexMetrics.chunkSizeZ == HexMetrics.chunkSizeZ - 1)
            {
                for (int i = 0; i <= (int)GridCubeCoordDirection.Northwest; i++)
                {
                    HexCell neighbor = grid.GetCell(cell.coordinates + GridCubeCoord.neighborList[i]);
                    if (neighbor)
                    {
                        (int x, int y) neighborChunkCoords = (neighbor.coordinates.x / HexMetrics.chunkSizeX, neighbor.coordinates.y / HexMetrics.chunkSizeZ);
                        if (!editedChunkCoordList.Contains(neighborChunkCoords))
                            editedChunkCoordList.Add(neighborChunkCoords);
                    }
                }
            }

            switch (mode_selected)
            {
                case HexEditorMode.Height:
                    {
                        EditCellHeight(cell);
                        break;
                    }
                case HexEditorMode.Texture:
                    {
                        EditCellColor(cell);
                        break;
                    }
            }
        }
    }

    void EditCellHeight(HexCell cell)
    {
        int newHeight = 0;
        switch (heightMode)
        {
            case HeightMode.Add:
                newHeight = Mathf.Min(256, (cell.Elevation + 1));
                break;
            case HeightMode.Subtract:
                newHeight = Mathf.Max(0, (cell.Elevation - 1));
                break;
            case HeightMode.Set:
                newHeight = height;
                break;
            case HeightMode.Smooth:
                newHeight = cell.Elevation == smoothTargetHeight ? cell.Elevation :
                    cell.Elevation > smoothTargetHeight ? cell.Elevation - 1 : cell.Elevation + 1;
                break;
            case HeightMode.Flatten:
                newHeight = cell.Elevation == flattenTargetHeight ? cell.Elevation :
                    cell.Elevation > flattenTargetHeight ? cell.Elevation - 1 : cell.Elevation + 1;
                break;
        }

        if (newHeight != cell.Elevation)
        {
            Undo.RecordObject(cell, UNDO_ELEVATION);
            cell.Elevation = newHeight;
        }
    }

    void EditCellColor(HexCell cell)
    {
        if (terrainTypeIndex != cell.TerrainTypeIndex)
        {
            Undo.RecordObject(cell, UNDO_COLOR);
            cell.TerrainTypeIndex = terrainTypeIndex;
        }
    }

    void UpdateChunks()
    {
        foreach (var chunkCoord in editedChunkCoordList)
        {
            grid.GetChunk(chunkCoord.x, chunkCoord.y)?.Refresh();
        }
    }

    void CollectCells(Vector3 center)
    {
        currentCell = grid.GetCell((GridCoord)center);

        IEnumerable<GridCoord> coords = GridCoord.InRadius((GridCubeCoord)center, brushSize);

        cellsToEdit.Clear();

        foreach (var coord in coords)
        {
            // we don't support cell coords < 0
            if (coord.x < 0 || coord.y < 0)
                continue;

            HexCell cell = grid.GetCell(coord);

            if (cell == null)
                continue;

            cellsToEdit.Add(cell);
        }
    }

    void DrawHandles(Vector3 center)
    {
        Handles.color = editing ? Color.green : Color.yellow;

        foreach (var cell in cellsToEdit)
        {
            Vector3 pos = cell.coordinates;
            pos.y = cell.Elevation * HexMetrics.elevationAmplitude;

            for (int i = 0, j = 0; i < 6; i++, j += 2)
            {
                Handles.DrawLine(GridCubeCoord.cornerList[i] + pos, GridCubeCoord.cornerList[(i + 1) % 6] + pos, 3f);
            }
        }

        Handles.DrawWireDisc(center, Vector3.up, GridCubeCoord.Size * (brushSize + 1), 3f);
    }

    bool RaycastHexMap(out Vector3 center)
    {
        HexGridChunk[] chunks = grid.GetChunks();

        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        RaycastHit hit;
        center = Vector3.zero;

        foreach (var chunk in chunks)
        {
            var collider = chunk.GetComponent<MeshCollider>();
            if (collider && collider.Raycast(ray, out hit, 1000f))
            {
                center = hit.point;
                SceneView.RepaintAll();
                return true;
            }
        }

        return false;
    }

    void CreateMapGUI()
    {
        if (GUILayout.Button("Create new Map"))
        {
            if (EditorUtility.DisplayDialog("Create a new map?", "Are you sure you want to create a new hex map?", "Create", "Cancel"))
            {
                grid.Clear();
                grid.Init();
            }
        }
    }

    void MapEditModeGUI()
    {
        mode_selected = (HexEditorMode)GUILayout.Toolbar((int)mode_selected, mode_names);

        GUILayout.Space(10);

        switch (mode_selected)
        {
            case HexEditorMode.Height:
                {
                    HeightModeGUI();
                    break;
                }
            case HexEditorMode.Texture:
                {
                    TextureModeGUI();
                    break;
                }
        }
    }

    void HeightModeGUI()
    {
        heightMode = (HeightMode)GUILayout.Toolbar((int)heightMode, heightEditingModes);

        switch (heightMode)
        {
            case HeightMode.Set:
                height = EditorGUILayout.IntSlider("Height", height, 0, 255);
                break;
        }
    }

    void TextureModeGUI()
    {
        terrainTypeIndex = EditorGUILayout.Popup(terrainTypeIndex, terrainTypes);
    }

    void SaveLoadMapGUI()
    {
        // Collect saves
        string[] guids = AssetDatabase.FindAssets("t:HexMapSave");
        string[] paths = new string[guids.Length];
        string[] names = new string[guids.Length + 1];
        names[0] = "None";
        for (int i = 0; i < guids.Length; i++)
        {
            paths[i] = AssetDatabase.GUIDToAssetPath(guids[i]);
            names[i + 1] = paths[i].Replace("/", "\\");
        }

        loadSaveMapSelection = EditorGUILayout.Popup(loadSaveMapSelection, names);

        // Save map
        GUILayout.BeginHorizontal();
        if (GUILayout.Button(loadSaveMapSelection == 0 ? "Save Map" : "Overwrite"))
        {
            if (loadSaveMapSelection == 0)
            {
                string path = EditorUtility.SaveFilePanelInProject("Save Map File", "Map", "asset", "Save Map File");
                if (path.Length != 0)
                {
                    HexMapSave save = HexMapSave.CreateHexMapSave(grid);
                    AssetDatabase.CreateAsset(save, path);
                }
            }
            else
            {
                if (EditorUtility.DisplayDialog("Overwrite saved map", "Are you sure that you want to overwrite the map", "Overwrite", "Cancel"))
                {
                    HexMapSave save = HexMapSave.CreateHexMapSave(grid);
                    AssetDatabase.CreateAsset(save, paths[loadSaveMapSelection - 1]);
                }
            }

        }

        // Load map
        if (paths.Length == 0)
            GUI.enabled = false;

        if (GUILayout.Button("Load Map") && loadSaveMapSelection != -1)
        {
            HexMapSave save = AssetDatabase.LoadAssetAtPath(paths[loadSaveMapSelection - 1], typeof(HexMapSave)) as HexMapSave;

            if (save)
            {
                grid.Load(save);
            }
            else
            {
                Debug.Log("Loading map file failed.");
            }
        }

        GUI.enabled = true;
        GUILayout.EndHorizontal();
    }

    void VisualizeBlockFlagsGUI()
    {
        if (visualizeBlockFlags && GUILayout.Button("Hide block flags"))
        {
            visualizeBlockFlags = false;
        }
        else if (!visualizeBlockFlags && GUILayout.Button("Show block flags"))
        {
            visualizeBlockFlags = true;
        }
    }

    void PrepareEditCells()
    {
        if (mode_selected == HexEditorMode.Height)
        {
            switch (heightMode)
            {
                case HeightMode.Smooth:
                    {
                        smoothTargetHeight = cellsToEdit.Sum(x => x.Elevation);
                        smoothTargetHeight /= cellsToEdit.Count;
                        break;
                    }
            }
        }
    }

    void BeginEditCells()
    {
        if (mode_selected == HexEditorMode.Height)
        {
            switch (heightMode)
            {
                case HeightMode.Flatten:
                    {
                        flattenTargetHeight = currentCell.Elevation;
                        break;
                    }
            }
        }
    }

    void UndoRedoPerformed()
    {
        grid.UpdateAllChunks();
    }

    void DrawBlockFlags()
    {
        foreach (var cell in grid.GetHexCells())
        {
            Vector3 pos = SceneView.currentDrawingSceneView.camera.WorldToViewportPoint(cell.coordinates);
            if (pos.x > 0f && pos.x < 1f && pos.y > 0f && pos.y < 1f)
            {
                for (int i = 0; i < 6; i++)
                {
                    bool blocked = cell.BlockDirFlags.HasFlag((GridCubeCoordBlockDirFlags)(1 << i));

                    Vector3 start = cell.coordinates + HexMetrics.GetFirstSolidCorner((GridCubeCoordDirection)i);
                    start.y = cell.Elevation * HexMetrics.elevationAmplitude + 0.1f;
                    Vector3 end = cell.coordinates + HexMetrics.GetSecondSolidCorner((GridCubeCoordDirection)i);
                    end.y = cell.Elevation * HexMetrics.elevationAmplitude + 0.1f;
                    Handles.color = blocked ? Color.red : Color.green;
                    Handles.DrawLine(start, end);
                }
            }
        }
    }
}