using System;
using UnityEngine;

namespace ProjectHunt
{
    [Serializable]
    public class HexCell : ScriptableObject
    {
        public GridCoord coordinates;

        GridCubeCoordBlockDirFlags blockedDirections = GridCubeCoordBlockDirFlags.None;
        public GridCubeCoordBlockDirFlags BlockDirFlags
        {
            get { return blockedDirections; }
            set { blockedDirections = value; }
        }

        [SerializeField]
        private int terrainTypeIndex;
        public int TerrainTypeIndex
        {
            get { return terrainTypeIndex; }
            set
            {
                if (terrainTypeIndex != value)
                    terrainTypeIndex = value;
            }
        }

        [SerializeField]
        private int elevation;
        public int Elevation
        {
            get { return elevation; }
            set
            {
                if (elevation != value)
                    elevation = value;
            }
        }
    }
}