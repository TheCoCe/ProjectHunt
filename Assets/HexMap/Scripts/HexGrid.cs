using System.Collections.Generic;
using UnityEngine;
using ProjectHunt.MapGeneration;
using ProjectHunt.HexTools;
using System.Collections.ObjectModel;

namespace ProjectHunt
{
    [RequireComponent(typeof(HexGridPlaceableManager))]
    [ExecuteInEditMode]
    public class HexGrid : MonoBehaviour
    {
        public int chunkCountX = 3, chunkCountZ = 3;
        private int cellCountX, cellCountZ;
        public HexGridChunk chunkPrefab;
        
        public int maxUpElevationDiff = 5;
        public int maxDownElevationDiff = 15;

        [SerializeField] [HideInInspector] HexCell[] cellsArray;
        [SerializeField] [HideInInspector] HexGridChunk[] chunksArray;
        [SerializeField] [HideInInspector] HexGridPlaceableManager placeableManager;
        
        public BaseMapLayoutGeneration mapLayoutGeneration;

        private Dictionary<GridCubeCoord, Unit> units;
        
        public RuntimeUnits runtimeUnits;

        public Pathfinding Pathfinding { get; private set; }

        void OnEnable()
        {
            //UpdateCellCount();
        }

        void Awake()
        {
            UpdateCellCount();
            UpdateBlockFlags();
            units = new Dictionary<GridCubeCoord, Unit>();

            Pathfinding = new Pathfinding(this, cellCountX, cellCountZ);
        }

        public void Init()
        {
            UpdateCellCount();
            CreateCells();
            GenerateMap();
            CreateChunks();
        }
        
        private void GenerateMap()
        {
            if (!mapLayoutGeneration)
                mapLayoutGeneration = ScriptableObject.CreateInstance<BaseMapLayoutGeneration>();

            mapLayoutGeneration.Init(cellCountX, cellCountZ);
            MapLayoutDefinition[] map = mapLayoutGeneration.Generate();

            for (int i = 0; i < map.Length; i++)
            {
                if (map[i] == MapLayoutDefinition.FILLED)
                {
                    cellsArray[i].Elevation = 40;
                    cellsArray[i].TerrainTypeIndex = 1;
                }
            }
        }
        
        private void UpdateBlockFlags()
        {
            placeableManager = GetComponent<HexGridPlaceableManager>();
            placeableManager.Init();
            placeableManager.UpdateHexGrid(this);
            SetBlockFlagsFromHeight();
        }

        private void SetBlockFlagsFromHeight()
        {
            foreach (var cell in cellsArray)
            {
                for (int i = 0; i < 6; i++)
                {
                    GridCoord neighbourCoord = cell.coordinates + GridCubeCoord.neighborList[i];
                    HexCell neighbourCell = GetCell(neighbourCoord);

                    if (!neighbourCell)
                        continue;

                    float elevationDiff = Mathf.Abs(cell.Elevation - neighbourCell.Elevation);
                    if (cell.Elevation > neighbourCell.Elevation && elevationDiff > maxDownElevationDiff)
                    {
                        cell.BlockDirFlags = cell.BlockDirFlags.SetFlagForDirection((GridCubeCoordDirection)i);
                    }
                    else if (cell.Elevation < neighbourCell.Elevation && elevationDiff > maxUpElevationDiff)
                    {
                        cell.BlockDirFlags = cell.BlockDirFlags.SetFlagForDirection((GridCubeCoordDirection)i);
                    }
                }
            }
        }

        void UpdateCellCount()
        {
            cellCountX = chunkCountX * HexMetrics.chunkSizeX;
            cellCountZ = chunkCountZ * HexMetrics.chunkSizeZ;
        }

        public void Load(HexMapSave save)
        {
            if (HexMapSave.Validate(save, this))
            {
                Clear();

                chunkCountX = save.chunkCountX;
                chunkCountZ = save.chunkCountZ;
                cellCountX = chunkCountX * HexMetrics.chunkSizeX;
                cellCountZ = chunkCountZ * HexMetrics.chunkSizeZ;

                cellsArray = new HexCell[cellCountX * cellCountZ];

                for (int i = 0; i < cellsArray.Length; i++)
                {
                    cellsArray[i] = ScriptableObject.CreateInstance<HexCell>();
                    cellsArray[i].coordinates = save.coordinates[i];
                    cellsArray[i].Elevation = save.elevations[i];
                    cellsArray[i].TerrainTypeIndex = save.terrainTypes[i];
                }

                CreateChunks();
            }
        }

        void CreateChunks()
        {
            chunksArray = new HexGridChunk[chunkCountX * chunkCountZ];

            for (int z = 0, i = 0; z < chunkCountZ; z++)
            {
                for (int x = 0; x < chunkCountX; x++)
                {
                    HexGridChunk chunk = chunksArray[i++] = Instantiate<HexGridChunk>(chunkPrefab);
                    chunk.transform.SetParent(transform);
                    chunk.Init(x, z, this);
                }
            }
        }

        void CreateCells()
        {
            cellsArray = new HexCell[cellCountX * cellCountZ];

            for (int z = 0, i = 0; z < cellCountZ; z++)
            {
                for (int x = 0; x < cellCountX; x++)
                {
                    CreateCell(x, z, i++);
                }
            }
        }

        void CreateCell(int x, int z, int i)
        {
            HexCell cell = ScriptableObject.CreateInstance<HexCell>();
            cell.coordinates = new GridCoord(x, z);
            cellsArray[cell.coordinates.x + cellCountX * cell.coordinates.y] = cell;

            cell.TerrainTypeIndex = 0;
            cell.Elevation = (int)((HexMetrics.SampleNoiseSingle(cell.coordinates, 0.025f) * 2f - 1f) * 10);
        }

        public HexCell GetCell(GridCoord coord)
        {
            // outside of the grid 
            if (coord.x >= cellCountX || coord.y >= cellCountZ || coord.x < 0 || coord.y < 0)
                return null;

            int idx = coord.x + cellCountX * coord.y;

            if (idx >= 0 && idx < cellsArray.Length)
                return cellsArray[idx];

            return null;
        }

        public HexCell GetCell(Vector3 pos)
        {
            GridCoord coord = (GridCubeCoord)pos;
            return GetCell(coord);
        }

        public bool HasCell(GridCoord coord)
        {
            if (coord.x >= cellCountX || coord.y >= cellCountZ || coord.x < 0 || coord.y < 0)
                return false;

            int idx = coord.x + cellCountX * coord.y;

            if (idx >= 0 && idx < cellsArray.Length)
                return true;

            return false;
        }

        public HexGridChunk GetChunk(int x, int y)
        {
            int idx = x + chunkCountX * y;
            if (idx >= 0 && idx < chunksArray.Length)
                return chunksArray[idx];
            else
                return null;
        }

        public HexGridChunk[] GetChunks()
        {
            return chunksArray;
        }

        public void UpdateAllChunks()
        {
            foreach (var chunk in chunksArray)
            {
                chunk.Refresh();
            }
        }

        public void Clear()
        {
            for (int i = transform.childCount; i > 0; --i)
            {
                DestroyImmediate(transform.GetChild(0).gameObject);
            }
        }

        public ReadOnlyCollection<HexCell> GetHexCells()
        {
            return new ReadOnlyCollection<HexCell>(cellsArray);
        }

        public int CellCount()
        {
            return cellsArray.Length;
        }

        public bool RaycastGround(out HexCell cellHit)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            cellHit = null;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("Ground")))
            {
                cellHit = GetCell(hit.point);
                if (cellHit)
                    return true;
            }

            return false;
        }
    }
}
