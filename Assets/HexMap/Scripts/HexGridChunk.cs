using UnityEngine;
using System.Collections.Generic;

namespace ProjectHunt.HexTools
{
    public class HexGridChunk : MonoBehaviour
    {
        [SerializeField]
        int x, z;
        public int X
        {
            get
            {
                return x;
            }
        }
        public int Z
        {
            get
            {
                return z;
            }
        }

        [SerializeField]
        HexGrid grid;
        [SerializeField]
        HexMesh hexMesh;

        public void Init(int x, int z, HexGrid grid)
        {
            this.grid = grid;
            this.x = x;
            this.z = z;

            hexMesh = GetComponent<HexMesh>();
            hexMesh.Init(grid);

            Refresh();
        }

        public void Refresh()
        {
            hexMesh.Triangulate(CollectChunkCells());
        }

        List<HexCell> CollectChunkCells()
        {
            List<HexCell> cells = new List<HexCell>();

            int offsetX, offsetZ;
            offsetX = x * HexMetrics.chunkSizeX;
            offsetZ = z * HexMetrics.chunkSizeZ;

            for (int x = 0, i = 0; x < HexMetrics.chunkSizeX; x++)
            {
                for (int z = 0; z < HexMetrics.chunkSizeZ; z++, i++)
                {
                    HexCell cell = grid.GetCell(new GridCoord(offsetX + x, offsetZ + z));
                    if (cell)
                    {
                        cells.Add(cell);
                    }
                }
            }

            return cells;
        }
    }
}