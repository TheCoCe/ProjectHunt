using UnityEngine;
using System.Collections.ObjectModel;

namespace ProjectHunt.HexTools
{
    public class HexMapSave : ScriptableObject
    {

        [HideInInspector] public int chunkCountX, chunkCountZ;
        [HideInInspector] public int chunkSizeX, chunkSizeZ;
        [SerializeField] [HideInInspector] private int length;
        public int Length
        {
            get { return length; }
        }
        [HideInInspector] public GridCoord[] coordinates;
        [HideInInspector] public int[] elevations;
        [HideInInspector] public int[] terrainTypes;

        private void Init(HexGrid grid)
        {
            chunkCountX = grid.chunkCountX;
            chunkCountZ = grid.chunkCountZ;

            chunkSizeX = HexMetrics.chunkSizeX;
            chunkSizeZ = HexMetrics.chunkSizeZ;

            ReadOnlyCollection<HexCell> cells = grid.GetHexCells();

            length = cells.Count;

            coordinates = new GridCoord[cells.Count];
            elevations = new int[cells.Count];
            terrainTypes = new int[cells.Count];

            for (int i = 0; i < cells.Count; i++)
            {
                HexCell cell = cells[i];
                coordinates[i] = cell.coordinates;
                elevations[i] = cell.Elevation;
                terrainTypes[i] = cell.TerrainTypeIndex;
            }
        }

        public static HexMapSave CreateHexMapSave(HexGrid grid)
        {
            HexMapSave save = ScriptableObject.CreateInstance<HexMapSave>();
            save.Init(grid);
            return save;
        }

        public static bool Validate(HexMapSave save, HexGrid grid)
        {
            if (save.coordinates.Length != save.Length || save.elevations.Length != save.Length || save.terrainTypes.Length != save.Length)
            {
                Debug.Log("Save file invalid: array length doesn't match the saved length!");
                return false;
            }

            if (HexMetrics.chunkSizeX != save.chunkSizeX || HexMetrics.chunkSizeZ != save.chunkSizeZ)
            {
                Debug.Log($"Grid metrics don't match the saved file! File expects a chunk size of X: {save.chunkSizeX}, Z: {save.chunkSizeZ}");
                return false;
            }

            return true;
        }
    }
}