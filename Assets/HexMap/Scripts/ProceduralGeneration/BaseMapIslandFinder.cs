using UnityEngine;
using System.Collections.Generic;

namespace ProjectHunt.MapGeneration
{
    public struct MapArea
    {
        HashSet<GridCoord> coords;
        Bounds bounds;
        bool borderArea;

        public MapArea(HashSet<GridCoord> coords, Bounds bounds, bool borderArea = false)
        {
            this.coords = coords;
            this.bounds = bounds;
            this.borderArea = borderArea;
        }
    }

    public static class BaseMapIslandFinder
    {
        public static List<MapArea> FindRooms(MapLayoutDefinition[] map, int sizeX, int sizeY)
        {
            return FindAreas(map, sizeX, sizeY, MapLayoutDefinition.EMPTY);
        }

        static List<MapArea> FindAreas(MapLayoutDefinition[] map, int sizeX, int sizeY, MapLayoutDefinition match)
        {
            List<MapArea> areas = new List<MapArea>();

            MapLayoutDefinition[] buffer = new MapLayoutDefinition[map.Length];
            System.Array.Copy(map, buffer, map.Length);

            for (int i = 0; i < sizeX * sizeY; i++)
            {
                if (buffer[i] != match || IsBorder(i, sizeX, sizeY))
                    continue;

                var areaSet = FloodFill(buffer, sizeX, sizeY, GridCoord.FromIndex(i, sizeX), match);
                // Clear tiles found by floodFill
                if (areaSet.Count > 0)
                {
                    int xMin = int.MaxValue, yMin = int.MaxValue, xMax = int.MinValue, yMax = int.MinValue;
                    bool borders = false;
                    foreach (var coord in areaSet)
                    {
                        if (coord.x < xMin)
                            xMin = coord.x;
                        else if (coord.x > xMax)
                            xMax = coord.x;
                        if (coord.y < yMin)
                            yMin = coord.y;
                        else if (coord.y > yMax)
                            yMax = coord.y;

                        if (!borders && TouchesBorder(coord, sizeX, sizeY))
                            borders = true;

                        buffer[GridCoord.ToIndex(coord, sizeX, sizeY)] = MapLayoutDefinition.NULL;
                    }

                    Vector3 center = new Vector3(xMin + xMax - xMin, 0, yMin + yMax - yMin);
                    Vector3 size = new Vector3(xMax - xMin, 0, yMax - yMin);
                    Bounds bounds = new Bounds(center, size);
                    MapArea area = new MapArea(areaSet, bounds, borders);
                    areas.Add(area);
                }
            }

            return areas;
        }

        public static (List<MapArea> mapIslands, List<MapArea> mapRooms) FindRoomsAndIslands(MapLayoutDefinition[] map, int sizeX, int sizeY)
        {
            List<MapArea> islands = new List<MapArea>();
            List<MapArea> rooms = new List<MapArea>();

            MapLayoutDefinition[] buffer = new MapLayoutDefinition[map.Length];
            System.Array.Copy(map, buffer, map.Length);

            for (int i = 0; i < sizeX * sizeY; i++)
            {
                if (buffer[i] == MapLayoutDefinition.NULL || IsBorder(i, sizeX, sizeY))
                    continue;

                MapLayoutDefinition match = buffer[i];
                var areaSet = FloodFill(buffer, sizeX, sizeY, GridCoord.FromIndex(i, sizeX), match);
                // Clear tiles found by floodFill
                if (areaSet.Count > 0)
                {
                    int xMin = int.MaxValue, yMin = int.MaxValue, xMax = int.MinValue, yMax = int.MinValue;
                    bool borders = false;
                    foreach (var coord in areaSet)
                    {
                        if (coord.x < xMin)
                            xMin = coord.x;
                        else if (coord.x > xMax)
                            xMax = coord.x;
                        if (coord.y < yMin)
                            yMin = coord.y;
                        else if (coord.y > yMax)
                            yMax = coord.y;

                        if (!borders && TouchesBorder(coord, sizeX, sizeY))
                            borders = true;

                        buffer[GridCoord.ToIndex(coord, sizeX, sizeY)] = MapLayoutDefinition.NULL;
                    }

                    Vector3 center = new Vector3(xMin + xMax - xMin, 0, yMin + yMax - yMin);
                    Vector3 size = new Vector3(xMax - xMin, 0, yMax - yMin);
                    Bounds bounds = new Bounds(center, size);
                    MapArea area = new MapArea(areaSet, bounds, borders);

                    if (match == MapLayoutDefinition.FILLED)
                        islands.Add(area);
                    else
                        rooms.Add(area);
                }
            }

            return (islands, rooms);
        }

        public static HashSet<GridCoord> FloodFill(MapLayoutDefinition[] map, int sizeX, int sizeY, GridCoord start, MapLayoutDefinition match)
        {
            Queue<GridCoord> frontier = new Queue<GridCoord>();
            frontier.Enqueue(start);
            HashSet<GridCoord> reached = new HashSet<GridCoord>();
            reached.Add(start);

            while (frontier.Count > 0)
            {
                GridCoord current = frontier.Dequeue();
                for (int i = 0; i < 6; i++)
                {
                    GridCoord newCoord = current + GridCubeCoord.neighborList[i];
                    int idx = GridCoord.ToIndex(newCoord, sizeX, sizeY);
                    if (idx > -1 && !IsBorder(idx, sizeX, sizeY) && map[idx] == match)
                    {
                        if (!reached.Contains(newCoord))
                        {
                            frontier.Enqueue(newCoord);
                            reached.Add(newCoord);
                        }
                    }
                }
            }

            return reached;
        }

        static bool IsBorder(int idx, int sizeX, int sizeY)
        {
            int x = idx % sizeX;
            int y = idx / sizeX;
            if (x == 0 || x == sizeX - 1 || y == 0 || y == sizeY - 1)
                return true;
            return false;
        }

        static bool TouchesBorder(GridCoord coord, int sizeX, int sizeY)
        {
            if (coord.x <= 1 || (coord.x >= sizeX - 2 && coord.x < sizeX) ||
                coord.y <= 1 || (coord.y >= sizeY - 2 && coord.y < sizeY))
                return true;
            return false;
        }
    }
}