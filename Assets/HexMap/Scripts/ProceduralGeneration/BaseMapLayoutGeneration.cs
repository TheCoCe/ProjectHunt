using UnityEngine;
using System.Collections.Generic;

namespace ProjectHunt.MapGeneration
{
    [System.Serializable]
    public struct BaseMapIteration
    {
        public BaseMapIteration(int min, int max, int rounds)
        {
            this.min = min;
            this.max = max;
            this.rounds = rounds;
        }

        public int min;
        public int max;
        public int rounds;
    }

    public enum MapLayoutDefinition : byte
    {
        NULL,
        EMPTY,
        FILLED
    }

    public enum MapGenType
    {
        Cellular,
        PerlinSimple,
        Perlin,
        Simplex,
    }

    [CreateAssetMenu(fileName = "Base Map Generation", menuName = "ProjectHunt/HexGrid/Base Map Generation")]
    public class BaseMapLayoutGeneration : ScriptableObject
    {
        #region DATA
        private int width = 100;
        private int height = 100;
        private int cellCount;

        [SerializeField] private MapGenType genType = MapGenType.Cellular;
        public MapGenType GenerationType { get; set; }

        // Cellular
        public float startFilledPercent = 0.4f;
        [SerializeField] private BaseMapIteration[] iterations;
        public BaseMapIteration[] Iterations
        {
            get { return iterations; }
            set { iterations = value; }
        }

        // Perlin
        [Range(0f, 1f)]
        public float perlinThreshold = 0.4f;
        [Range(0f, 10f)]
        public float perlinScale = 1f;
        private float randomPerlinOffset = 0f;

        // Simplex
        [Range(0f, 1f)]
        public float simplexThreshold = 0.4f;
        [Range(0f, 10f)]
        public float simplexScale = 1f;
        private FastNoiseLite fastNoiseLite;

        public int octaves = 1;

        // General
        [SerializeField] private int seed = 0;
        public int Seed
        {
            get { return seed; }
            set { seed = value; }
        }

        private MapLayoutDefinition[] map;
        private MapLayoutDefinition[] buffer;
        #endregion

        public void Init(int width, int height)
        {
            this.width = width;
            this.height = height;

            switch (genType)
            {
                case MapGenType.Cellular:
                    InitMapCellular();
                    break;
                case MapGenType.PerlinSimple:
                    InitMapPerlinSimple();
                    break;
                case MapGenType.Perlin:
                    InitMapPerlin();
                    break;
                case MapGenType.Simplex:
                    InitMapSimplex();
                    break;
            }
        }

        public MapLayoutDefinition[] Generate()
        {
            switch (genType)
            {
                default:
                case MapGenType.Cellular:
                    return GenerateCellular();
                case MapGenType.PerlinSimple:
                    return GeneratePerlinSimple();
                case MapGenType.Perlin:
                    return GeneratePerlin();
                case MapGenType.Simplex:
                    return GenerateSimplex();
            }
        }

        #region CELLULAR
        MapLayoutDefinition[] GenerateCellular()
        {
            Iterate();
            var rooms = BaseMapIslandFinder.FindRooms(map, width, height);
            if (rooms.Count > 1)
            {

            }
            return map;
        }

        void InitMapCellular()
        {
            cellCount = width * height;

            map = new MapLayoutDefinition[cellCount];
            buffer = new MapLayoutDefinition[cellCount];

            Random.InitState(seed);

            for (int i = 0; i < cellCount; i++)
            {
                buffer[i] = MapLayoutDefinition.FILLED;
                map[i] = MapLayoutDefinition.EMPTY;

                if (i % width == 0 || i % width == width - 1 || i < width || i > cellCount - width)
                {
                    map[i] = MapLayoutDefinition.FILLED;
                    continue;
                }

                if (this.startFilledPercent >= Random.value)
                {
                    map[i] = MapLayoutDefinition.FILLED;
                }
            }
        }

        void Step(int min, int max)
        {
            for (int i = 0; i < cellCount; i++)
            {
                int count1 = GetNeighbourCount(i, 1);
                int count2 = GetNeighbourCount(i, 2, true);

                if (count1 >= min || count2 <= max)
                    buffer[i] = MapLayoutDefinition.FILLED;
                else
                    buffer[i] = MapLayoutDefinition.EMPTY;
            }
            MapLayoutDefinition[] tempMap = map;
            map = buffer;
            buffer = tempMap;
        }

        // alternate version by Sebastian Lague
        void Step2(int min, int max)
        {
            for (int i = 0; i < cellCount; i++)
            {
                int count = GetNeighbourCount(i, 1);

                if (count > min)
                    buffer[i] = MapLayoutDefinition.FILLED;
                else if (count < max)
                    buffer[i] = MapLayoutDefinition.EMPTY;
            }
            MapLayoutDefinition[] tempMap = map;
            map = buffer;
            buffer = tempMap;
        }

        // using moore neighbourhood
        // TODO: try von neumann neighbourhood as well?
        int GetNeighbourCount(int index, int distance, bool ignoreCorners = false)
        {
            if (index < 0 || index >= cellCount)
                return -1;

            int count = 0;
            int x = index % width;
            int y = index / width;

            for (int xx = -distance; xx <= distance; xx++)
            {
                for (int yy = -distance; yy <= distance; yy++)
                {
                    if (ignoreCorners && xx == yy)
                        continue;
                    if (x + xx >= 0 && y + yy >= 0 && x + xx < width && y + yy < height)
                    {
                        if (map[index + xx + yy * width] == MapLayoutDefinition.FILLED) count++;
                    }
                    else
                        count++;
                }
            }

            return count;
        }

        void Iterate()
        {
            foreach (var iteration in iterations)
            {
                for (int i = 0; i < iteration.rounds; i++)
                {
                    Step(iteration.min, iteration.max);
                    //Debug.Log($"Round: {i}");
                    //Debug.Log(this);
                }
            }
        }
        #endregion

        #region PERLIN
        void InitMapPerlinSimple()
        {
            cellCount = width * height;

            map = new MapLayoutDefinition[cellCount];

            Random.InitState(seed);
            randomPerlinOffset = Random.Range(0f, 1_000_000f);

            for (int i = 0; i < cellCount; i++)
            {
                map[i] = MapLayoutDefinition.EMPTY;

                if (i % width == 0 || i % width == width - 1 || i < width || i > cellCount - width)
                {
                    map[i] = MapLayoutDefinition.FILLED;
                }
            }
        }

        void InitMapPerlin()
        {
            cellCount = width * height;

            map = new MapLayoutDefinition[cellCount];

            fastNoiseLite = new FastNoiseLite(seed);
            fastNoiseLite.SetNoiseType(FastNoiseLite.NoiseType.Perlin);
            fastNoiseLite.SetFractalType(FastNoiseLite.FractalType.Ridged);
            fastNoiseLite.SetFractalOctaves(octaves);

            for (int i = 0; i < cellCount; i++)
            {
                map[i] = MapLayoutDefinition.EMPTY;

                if (i % width == 0 || i % width == width - 1 || i < width || i > cellCount - width)
                {
                    map[i] = MapLayoutDefinition.FILLED;
                }
            }
        }

        MapLayoutDefinition[] GeneratePerlinSimple()
        {
            for (int i = 0; i < cellCount; i++)
            {
                if (i % width == 0 || i % width == width - 1 || i < width || i > cellCount - width)
                    continue;

                float x = (i % width) * perlinScale + randomPerlinOffset;
                float y = (i / width) * perlinScale + randomPerlinOffset;
                if (Mathf.PerlinNoise(x, y) > perlinThreshold)
                {
                    map[i] = MapLayoutDefinition.FILLED;
                }
                else
                {
                    map[i] = MapLayoutDefinition.EMPTY;
                }
            }

            return map;
        }

        MapLayoutDefinition[] GeneratePerlin()
        {
            for (int i = 0; i < cellCount; i++)
            {
                if (i % width == 0 || i % width == width - 1 || i < width || i > cellCount - width)
                    continue;

                float x = (i % width) * perlinScale;
                float y = (i / width) * perlinScale;
                if ((fastNoiseLite.GetNoise(x, y) + 1f) * 0.5f > perlinThreshold)
                {
                    map[i] = MapLayoutDefinition.FILLED;
                }
                else
                {
                    map[i] = MapLayoutDefinition.EMPTY;
                }
            }

            return map;
        }
        #endregion

        #region SIMPLEX
        void InitMapSimplex()
        {
            cellCount = width * height;

            map = new MapLayoutDefinition[cellCount];

            fastNoiseLite = new FastNoiseLite(seed);
            fastNoiseLite.SetNoiseType(FastNoiseLite.NoiseType.OpenSimplex2);
            fastNoiseLite.SetFractalType(FastNoiseLite.FractalType.Ridged);
            fastNoiseLite.SetFractalOctaves(octaves);

            for (int i = 0; i < cellCount; i++)
            {
                map[i] = MapLayoutDefinition.EMPTY;

                if (i % width == 0 || i % width == width - 1 || i < width || i > cellCount - width)
                {
                    map[i] = MapLayoutDefinition.FILLED;
                }
            }
        }

        MapLayoutDefinition[] GenerateSimplex()
        {
            for (int i = 0; i < cellCount; i++)
            {
                if (i % width == 0 || i % width == width - 1 || i < width || i > cellCount - width)
                    continue;

                float x = (i % width) * simplexScale;
                float y = (i / width) * simplexScale;
                if ((fastNoiseLite.GetNoise(x, y) + 1f) * 0.5f > simplexThreshold)
                {
                    map[i] = MapLayoutDefinition.FILLED;
                }
                else
                {
                    map[i] = MapLayoutDefinition.EMPTY;
                }
            }

            return map;
        }
        #endregion

        void FixRooms(List<MapArea> rooms)
        {
            //TODO: connect rooms
            // Find closest room via bounding box of other rooms
            // Find closest cell to center and start walking towards the closest cell in the closest room
            // If center is already part of the room we can just start walking from there

            if (rooms.Count <= 1)
                return;

            for (int i = 1; i < rooms.Count; i++)
            {

            }
        }

        #region ClassStuff
        public override string ToString()
        {
            string mapString = "";
            for (int i = 0; i < cellCount; i++)
            {
                mapString += this.map[i] == MapLayoutDefinition.FILLED ? "+" : " ";
                if (i % width == 0) mapString += "\n";
            }

            return mapString;
        }
        #endregion
    }
}