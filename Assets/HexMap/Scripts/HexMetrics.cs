using UnityEngine;

namespace ProjectHunt.HexTools
{
    public static class HexMetrics
    {
        public static int chunkSizeX = 10, chunkSizeZ = 10;

        public static float solidFactor = 0.8f;

        public static float blendFactor = 1f - solidFactor;

        public static float elevationAmplitude = 0.1f;

        public static float cellPerturbStrength = 0.2f;

        public static float noiseScale = 1f;

        public static float elevationPerturbStrength = 0.5f;

        private static float noiseLayerOffset = 100f;

        public static Vector3 GetFirstCorner(GridCubeCoordDirection direction)
        {
            return GridCubeCoord.cornerList[(int)direction.Previous()];
        }

        public static Vector3 GetSecondCorner(GridCubeCoordDirection direction)
        {
            return GridCubeCoord.cornerList[(int)direction];
        }

        public static Vector3 GetFirstSolidCorner(GridCubeCoordDirection direction)
        {
            return GridCubeCoord.cornerList[(int)direction.Previous()] * solidFactor;
        }

        public static Vector3 GetSecondSolidCorner(GridCubeCoordDirection direction)
        {
            return GridCubeCoord.cornerList[(int)direction] * solidFactor;
        }

        public static Vector3 GetBridge(GridCubeCoordDirection direction)
        {
            return (GridCubeCoord.cornerList[(int)direction.Previous()] + GridCubeCoord.cornerList[(int)direction]) *
                blendFactor;
        }

        public static Vector3 SampleNoise(Vector3 position)
        {
            Vector3 noise;
            noise.x = Mathf.PerlinNoise(position.x * noiseScale, position.z * noiseScale);
            noise.y = Mathf.PerlinNoise(position.x * noiseScale + noiseLayerOffset, position.z * noiseScale + noiseLayerOffset);
            noise.z = Mathf.PerlinNoise(position.x * noiseScale + 2f * noiseLayerOffset, position.z * noiseScale + 2f * noiseLayerOffset);
            return noise;
        }

        public static float SampleNoiseSingle(Vector3 position, float scale)
        {
            return Mathf.PerlinNoise(position.x * scale, position.z * scale);
        }
    }
}