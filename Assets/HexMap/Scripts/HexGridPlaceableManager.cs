using UnityEngine;

namespace ProjectHunt.HexTools
{
    public class HexGridPlaceableManager : MonoBehaviour
    {
        HexGridPlaceable[] placeables;

        public void Init()
        {
            // Add all placables that are present in the scene to the placeable manager
            placeables = FindObjectsOfType<HexGridPlaceable>();
        }

        public void UpdateHexGrid(HexGrid grid)
        {
            foreach (var placeable in placeables)
            {
                GridCubeCoord placeableCoord = (GridCoord)placeable.transform.position;

                int rotation = Mathf.RoundToInt(placeable.transform.rotation.eulerAngles.y / 60f) % 6;
                bool left = rotation < 0;
                rotation = Mathf.Abs(rotation);

                foreach (var blockFlag in placeable.placeableDefinition.blockCoords)
                {
                    GridCubeCoord rotatedCoord = GridCubeCoord.GetRotatedCoord(blockFlag.Key, rotation, left);
                    HexCell cell = grid.GetCell(placeableCoord + rotatedCoord);
                    if (cell)
                    {
                        cell.BlockDirFlags |= left ? blockFlag.Value.RotateLeft(rotation) : blockFlag.Value.RotateRight(rotation);
                    }
                }
            }
        }
    }
}