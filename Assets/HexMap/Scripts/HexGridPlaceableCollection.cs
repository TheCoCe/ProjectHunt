using UnityEngine;
using System.Collections.Generic;
using ProjectHunt.HexTools;

[System.Serializable]
public struct HexGridPlaceableCollectionItem
{
    public HexGridPlaceable placeable;
    public float probability;
}

[CreateAssetMenu(fileName = "Placeable Collection", menuName = "ProjectHunt/HexGrid/Placeable Collection")]
public class HexGridPlaceableCollection : ScriptableObject
{
    public List<HexGridPlaceableCollectionItem> placeableCollectionItems = new List<HexGridPlaceableCollectionItem>();
}