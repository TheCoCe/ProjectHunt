using System.Collections.Generic;
using UnityEngine;

namespace ProjectHunt.HexTools
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class HexMesh : MonoBehaviour
    {
        [SerializeField]
        Mesh hexMesh;
        static List<Vector3> vertices = new List<Vector3>();
        static List<int> triangles = new List<int>();
        static List<Color> colors = new List<Color>();
        static List<Vector3> terrainTypes = new List<Vector3>();

        static Color color1 = new Color(1f, 0f, 0f);
        static Color color2 = new Color(0f, 1f, 0f);
        static Color color3 = new Color(0f, 0f, 1f);

        [SerializeField]
        MeshCollider meshCollider;
        [SerializeField]
        HexGrid grid;

        public void Init(HexGrid grid)
        {
            this.grid = grid;
            gameObject.GetComponent<MeshFilter>().mesh = hexMesh = new Mesh();
            meshCollider = gameObject.GetComponent<MeshCollider>();
            hexMesh.name = "Hex mesh";
        }

        public void Triangulate(List<HexCell> cells)
        {
            Clear();

            foreach (var cell in cells)
            {
                Triangulate(cell);
            }

            Apply();
        }

        void Clear()
        {
            hexMesh.Clear();
            vertices.Clear();
            colors.Clear();
            triangles.Clear();
            terrainTypes.Clear();
        }

        void Apply()
        {
            hexMesh.vertices = vertices.ToArray();
            hexMesh.colors = colors.ToArray();
            hexMesh.triangles = triangles.ToArray();
            hexMesh.SetUVs(2, terrainTypes);

            hexMesh.RecalculateNormals();

            meshCollider.sharedMesh = hexMesh;
        }

        void Triangulate(HexCell cell)
        {
            Vector3 center = cell.coordinates;
            center.y = cell.Elevation * HexMetrics.elevationAmplitude;
            TriangulateHexMesh(center, cell.TerrainTypeIndex);

            for (GridCubeCoordDirection d = GridCubeCoordDirection.North; d <= GridCubeCoordDirection.Southeast; d++)
            {
                TriangulateConnection(d, cell, center);
            }
        }

        void TriangulateConnection(GridCubeCoordDirection direction, HexCell cell, Vector3 center)
        {
            EdgeVertices e1 = new EdgeVertices(center + HexMetrics.GetFirstSolidCorner(direction), center + HexMetrics.GetSecondSolidCorner(direction));
            GridCubeCoord neighborCoord = cell.coordinates + GridCubeCoord.neighborList[(int)direction];
            HexCell neighbor = grid.GetCell(neighborCoord);

            if (neighbor == null)
                return;

            Vector3 bridge = HexMetrics.GetBridge(direction);
            bridge.y = neighbor.Elevation * HexMetrics.elevationAmplitude - center.y;
            EdgeVertices e2 = new EdgeVertices(e1.v1 + bridge, e1.v4 + bridge);

            TriangulateEdgeStrip(e1, color1, cell.TerrainTypeIndex, e2, color2, neighbor.TerrainTypeIndex);

            HexCell nextNeighbor = grid.GetCell(cell.coordinates + GridCubeCoord.neighborList[(int)direction.Next()]);
            if (direction <= GridCubeCoordDirection.Northeast && nextNeighbor != null)
            {
                Vector3 v5 = e1.v4 + HexMetrics.GetBridge(direction.Next());
                v5.y = nextNeighbor.Elevation * HexMetrics.elevationAmplitude;
                AddTriangle(e1.v4, e2.v4, v5);
                AddTriangleColor(color1, color2, color3);
                AddTriangleTerrainTypes(cell.TerrainTypeIndex, neighbor.TerrainTypeIndex, nextNeighbor.TerrainTypeIndex);
            }
        }

        void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3)
        {
            int vertexIndex = vertices.Count;
            vertices.Add(Perturb(v1));
            vertices.Add(Perturb(v2));
            vertices.Add(Perturb(v3));
            triangles.Add(vertexIndex);
            triangles.Add(vertexIndex + 1);
            triangles.Add(vertexIndex + 2);
        }

        void AddTriangleColor(Color c1, Color c2, Color c3)
        {
            colors.Add(c1);
            colors.Add(c2);
            colors.Add(c3);
        }

        void AddTriangleColor(Color color)
        {
            colors.Add(color);
            colors.Add(color);
            colors.Add(color);
        }

        void AddQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4)
        {
            int vertexIndex = vertices.Count;
            vertices.Add(Perturb(v1));
            vertices.Add(Perturb(v2));
            vertices.Add(Perturb(v3));
            vertices.Add(Perturb(v4));
            triangles.Add(vertexIndex);
            triangles.Add(vertexIndex + 2);
            triangles.Add(vertexIndex + 1);
            triangles.Add(vertexIndex + 1);
            triangles.Add(vertexIndex + 2);
            triangles.Add(vertexIndex + 3);
        }

        void AddQuadColor(Color c1, Color c2, Color c3, Color c4)
        {
            colors.Add(c1);
            colors.Add(c2);
            colors.Add(c3);
            colors.Add(c4);
        }

        void AddQuadColor(Color c1, Color c2)
        {
            colors.Add(c1);
            colors.Add(c1);
            colors.Add(c2);
            colors.Add(c2);
        }

        Vector3 Perturb(Vector3 position)
        {
            Vector3 sample = HexMetrics.SampleNoise(position);
            position.x += (sample.x * 2f - 1f) * HexMetrics.cellPerturbStrength;
            //position.y += (sample.y * 2f - 1f) * HexMetrics.cellPerturbStrength;
            position.z += (sample.z * 2f - 1f) * HexMetrics.cellPerturbStrength;
            return position;
        }

        void TriangulateEdgeFan(Vector3 center, EdgeVertices edge, Color color)
        {
            AddTriangle(center, edge.v1, edge.v2);
            AddTriangleColor(color);
            AddTriangle(center, edge.v2, edge.v3);
            AddTriangleColor(color);
            AddTriangle(center, edge.v3, edge.v4);
            AddTriangleColor(color);
        }

        void TriangulateEdgeStrip(EdgeVertices e1, Color c1, float type1, EdgeVertices e2, Color c2, float type2)
        {
            AddQuad(e1.v1, e1.v2, e2.v1, e2.v2);
            AddQuad(e1.v2, e1.v3, e2.v2, e2.v3);
            AddQuad(e1.v3, e1.v4, e2.v3, e2.v4);

            AddQuadColor(c1, c2);
            AddQuadColor(c1, c2);
            AddQuadColor(c1, c2);

            Vector3 types;
            types.x = types.z = type1;
            types.y = type2;
            AddQuadTerrainTypes(types);
            AddQuadTerrainTypes(types);
            AddQuadTerrainTypes(types);
        }

        void TriangulateHexMesh(Vector3 center, float type)
        {
            Vector3 types;
            types.x = types.y = types.z = type;

            int centerIndex = vertices.Count;
            vertices.Add(center);
            colors.Add(color1);
            terrainTypes.Add(types);

            for (GridCubeCoordDirection d = GridCubeCoordDirection.North; d <= GridCubeCoordDirection.Northwest; d++)
            {
                Vector3 v1 = center + HexMetrics.GetFirstSolidCorner(d);
                Vector3 v2 = center + HexMetrics.GetSecondSolidCorner(d);
                Vector3 s1 = Vector3.Lerp(v1, v2, 1f / 3f);
                Vector3 s2 = Vector3.Lerp(v1, v2, 2f / 3f);
                vertices.Add(Perturb(v1));
                vertices.Add(Perturb(s1));
                vertices.Add(Perturb(s2));
                colors.Add(color1);
                colors.Add(color1);
                colors.Add(color1);
                terrainTypes.Add(types);
                terrainTypes.Add(types);
                terrainTypes.Add(types);
            }

            TriangulateHex(centerIndex);
        }

        void TriangulateHex(int centerIndex)
        {
            for (int i = 0; i < 18; i++)
            {
                triangles.Add(centerIndex);
                triangles.Add(centerIndex + (i + 1));
                triangles.Add(centerIndex + ((i + 2) > 18 ? 1 : i + 2));
            }
        }

        void AddQuadTerrainTypes(Vector3 types)
        {
            terrainTypes.Add(types);
            terrainTypes.Add(types);
            terrainTypes.Add(types);
            terrainTypes.Add(types);
        }

        void AddTriangleTerrainTypes(float t1, float t2, float t3)
        {
            Vector3 types;
            types.x = t1;
            types.y = t2;
            types.z = t3;
            terrainTypes.Add(types);
            terrainTypes.Add(types);
            terrainTypes.Add(types);
        }
    }
}