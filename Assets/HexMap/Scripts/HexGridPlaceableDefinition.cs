using UnityEngine;
using System;

namespace ProjectHunt.HexTools
{
    [CreateAssetMenu(fileName = "PlaceableDefinition", menuName = "HexGrid/HexGridPlaceableDefinition")]
    public class HexGridPlaceableDefinition : ScriptableObject
    {
        public PlaceableCoordDefinitionDictionary blockCoords;
    }

    [Serializable]
    public class PlaceableCoordDefinitionDictionary : SerializableDictionary<GridCoord, GridCubeCoordBlockDirFlags> { };

    public static class SerializableDictionaryExtension
    {
        public static void ChangeKey<TKey, TValue>(this SerializableDictionary<TKey, TValue> dict, TKey fromKey, TKey toKey)
        {
            TValue value = dict[fromKey];
            dict.Remove(fromKey);
            dict[toKey] = value;
        }
    }
}