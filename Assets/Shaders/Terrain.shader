Shader "Custom/Terrain"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("TerrainTextureArray", 2DArray) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _GridTex ("Grid Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert

        // Use shader model 3.5 target, to get nicer looking lighting and terrain texture support
        #pragma target 3.5

        UNITY_DECLARE_TEX2DARRAY(_MainTex);
        fixed4 _MainTex_ST;
        sampler2D _GridTex;

        struct Input
        {
            float4 color : COLOR;
            float3 worldPos;
            float3 terrain;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void vert (inout appdata_full v, out Input data)
        {
            UNITY_INITIALIZE_OUTPUT(Input, data);
            data.terrain = v.texcoord2.xyz;
        }

        float4 GetTerrainColor (Input IN, int index) {
            IN.worldPos.xz = TRANSFORM_TEX(IN.worldPos.xz, _MainTex);
            float3 uvw = float3(IN.worldPos.xz, IN.terrain[index]);
            float4 c = UNITY_SAMPLE_TEX2DARRAY(_MainTex, uvw);
            return c * IN.color[index];
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            //Hex Grid Lines
            float2 gridUV = IN.worldPos.xz;
			gridUV.y *= 1 / (4 * 0.866025404);
			gridUV.x *= 1 / 3.0;
            fixed4 grid = tex2D(_GridTex, gridUV);

            // Albedo comes from a texture tinted by color
            fixed4 c = GetTerrainColor(IN, 0) + GetTerrainColor(IN, 1) + GetTerrainColor(IN, 2);
            o.Albedo = c.rgb * grid * _Color;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
